﻿using System;
using System.ComponentModel;
using System.IO;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Pokemon.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class SoundboxPage : ContentPage
    {
        public bool rekt = true;

        
        public SoundboxPage()
        {
            InitializeComponent();
        }
        private void Invert(object sender, EventArgs e)
        {
            if (!rekt)
            {
                languageButton.Text = "Mode Soundbox";
            }
            else
            {
                languageButton.Text = "Mode Download";
            }
            rekt = !rekt;
        }
        private void StopSound_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().StopSoundFile();
        }
        #region imitation
        private void JaponaisFildrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/FildrongJapon.mp3");
        }
        private void FildrongSauvage_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/FildrongSauvage.mp3");
        }
        private void DjFildrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/DJFILDRONG.mp3");
        }
        private void Tutankadrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/Tutankadrong.mp3");
        }
        private void Taridrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/Taridrong.mp3");
        }
        private void Allemandrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/Allemandrong.mp3");
        }
        private void Donaldrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("imitation/Donaldrong.mp3");
        }
        #endregion

        #region expression
        private void BraguetteFildrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/Braguette.mp3");
        }
        private void CoupureDeCoran_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/CoupureDeCoran.mp3");
        }
        private void CrocFeu_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/CrocFeuv2.mp3");
        }
        private void Vodka_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/Vodka.mp3");
        }
        private void Nonnonnon_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/nonnonnon.mp3");
        }
        private void Sliptrouer_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/sliptrouer.mp3");
        }
        private void Gagner_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/gagner.mp3");
        }
        private void Ragedrong_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/ragedrong.mp3");
        }
        private void Clavier_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/clavier.mp3");
        }
        private void Katagami_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/katagami.mp3");
        }
        private void Dimoret_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/dimoret.mp3");
        }
        private void Chinoisrond_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/chinoisrond.mp3");
        }
        private void Pompier_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/Pompier.mp3");
        }
        private void Explosif_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/explosif.mp3");
        }
        private void Stall_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/stall.mp3");
        }
        private void Sommeil_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("autre/6tours.mp3");
        }
        #endregion

        #region alerte live
        private void DeTypeAcier_Clicked(object sender, EventArgs e)
        {
            if (rekt)
            {
                DependencyService.Get<ISoundPlayer>().PlaySoundFile("alerte/typeacier.mp3");
            }
            else
            {
                Browser.OpenAsync("https://drive.google.com/open?id=1nPDW8cNR3USEyeiYofn8u0yFXTYXlM2c");
            }
        }
        private void Reveiltoi_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("alerte/reveiltoi.mp3");
        }
        private void PrimoKGR_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("alerte/PrimoKGR.mp3");
        }
        private void Desillusion_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("alerte/desillusion.mp3");
        }
        private void Boule_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile("alerte/Boule.mp3");
        }
        #endregion
    }
}