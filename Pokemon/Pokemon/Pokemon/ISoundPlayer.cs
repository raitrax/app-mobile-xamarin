﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokemon
{
    public interface ISoundPlayer
    {
        void PlaySoundFile(string fileName);
        void StopSoundFile();
    }
}
