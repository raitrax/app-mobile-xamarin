﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokemon.Models
{
    public enum MenuItemType
    {
        Browse,
        AboutFildrong,
        AboutFriends,
        About,
        Soundbox
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
