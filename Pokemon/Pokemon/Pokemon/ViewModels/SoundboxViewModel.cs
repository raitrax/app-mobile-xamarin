﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Pokemon.ViewModels
{
    public class SoundboxViewModel : BaseViewModel
    {
        public SoundboxViewModel()
        {
            Title = "Soundbox";
            //Sons = new Command(async () => await Browser.OpenAsync("https://drive.google.com/open?id=1nPDW8cNR3USEyeiYofn8u0yFXTYXlM2c"));
            //Sons = new Command(async () => await invert()) ;
        
        }

        public ICommand OpenWebCommand { get; }
        public ICommand Sons { get; }
    }
}