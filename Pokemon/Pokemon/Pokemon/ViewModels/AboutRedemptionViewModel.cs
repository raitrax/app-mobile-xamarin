﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Pokemon.ViewModels
{
    public class AboutRedemptionViewModel : BaseViewModel
    {
        public AboutRedemptionViewModel()
        {
            Title = "A propos de Redemption";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UCAEhlJXdFkR9V5Sqeu-vptg"));
        }

        public ICommand OpenWebCommand { get; }
    }
}