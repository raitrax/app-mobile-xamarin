﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Pokemon.ViewModels
{
    public class AboutFildrongViewModel : BaseViewModel
    {
        public AboutFildrongViewModel()
        {
            Title = "A propos de Fildrong";
            Youtube = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UCO35rCYrO_3t6f4tEU83WKg"));
            Twitter = new Command(async () => await Browser.OpenAsync("https://twitter.com/Fildrong"));
            Twitch = new Command(async () => await Browser.OpenAsync("https://www.twitch.tv/fildrong"));
            Instagram = new Command(async () => await Browser.OpenAsync("https://www.instagram.com/fildronglevrai"));
            Facebook = new Command(async () => await Browser.OpenAsync("https://www.facebook.com/fildrong/"));
            Discord = new Command(async () => await Browser.OpenAsync("https://discord.gg/fildrong"));
            Showdown = new Command(async () => await Browser.OpenAsync("http://play.pokemonshowdown.com/salty"));
            Teespring = new Command(async () => await Browser.OpenAsync("https://teespring.com/stores/fildrong"));
        }

        public ICommand Youtube { get; }
        public ICommand Twitter { get; }
        public ICommand Twitch { get; }
        public ICommand Instagram { get; }
        public ICommand Facebook { get; } 
        public ICommand Discord { get; }
        public ICommand Showdown { get; }
        public ICommand Teespring { get; }
    }
}