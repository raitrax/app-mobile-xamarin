﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Pokemon.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "A Propos";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UCV1aAmztXWk-uM-nw57hFLQ"));
        }

        public ICommand OpenWebCommand { get; }
    }
}