﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Pokemon.ViewModels
{
    public class AboutFriendsViewModel : BaseViewModel
    {
        public AboutFriendsViewModel()
        {
            Title = "Les potes";
            //Red
            YoutubeRed = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UCAEhlJXdFkR9V5Sqeu-vptg"));

            //Maitre Armand
            YoutubeMA = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UCysbNzhNmpLCx4dqWNFzGgA"));

            //Sorcier Malgache
            YoutubeSM = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UC9E3zLroMFLvvTZt7JJyq_A"));

            //Hari
            YoutubeHa = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/channel/UCV1aAmztXWk-uM-nw57hFLQ"));

            //Pokemon Trash
            YoutubePT = new Command(async () => await Browser.OpenAsync("https://www.youtube.com/user/videopokemontrash"));
        }

        //Red
        public ICommand YoutubeRed { get; }

        //Sorcier
        public ICommand YoutubeSM { get; }

        //Armand
        public ICommand YoutubeMA { get; }

        //HARI
        public ICommand YoutubeHa { get; }

        //Pokemon Trash
        public ICommand YoutubePT { get; }

    }
}