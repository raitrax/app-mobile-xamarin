﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using System.IO;
using AVFoundation;
using Pokemon.iOS;

[assembly: Dependency(typeof(SoundPlayerImplementation))]
namespace Pokemon.iOS
{
    public class SoundPlayerImplementation : ISoundPlayer
    {
        // Need to have the iOS audio player scoped outside of the method so the playback can persist
        private AVAudioPlayer audioPlayer;

        public SoundPlayerImplementation()
        {

        }

        public void PlaySoundFile(string fileName)
        {
            // Retrieve the path to the file where it is housed within the bundle resources
            string sFilePath = NSBundle.MainBundle.PathForResource(Path.GetFileNameWithoutExtension(fileName), Path.GetExtension(fileName));
            // Create a URL for the file inside the bundle resources - we do this for simplicity in play back
            var url = NSUrl.FromString(((NSString)sFilePath).CreateStringByAddingPercentEscapes(NSStringEncoding.UTF8));

            // Since the audio player is scoped outside of the method, we need to handle what to do if it is already playing
            // a sound when they trigger another to play
            if (audioPlayer != null)
            {
                audioPlayer.Stop();
                audioPlayer.Dispose();
            }

            // Instantiate the AudioPlayer from the URL we created above
            audioPlayer = AVAudioPlayer.FromUrl(url);

            // Sanity check to make sure it was able to properly create the player
            if (audioPlayer != null)
            {
                // Set it for a single playback
                audioPlayer.NumberOfLoops = 0;

                // Add a delegate to perform some clean-up after it finishes playing
                audioPlayer.FinishedPlaying += delegate
                {
                    audioPlayer = null;
                };

                // Start playing the sound file
                audioPlayer.Play();
            }
        }
        public void StopSoundFile()
        {
            if (audioPlayer != null)
            {
                audioPlayer.DangerousRelease();
            }
        }
    }
}