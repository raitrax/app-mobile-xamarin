﻿using System.IO;
using AVFoundation;
using Foundation;
using Fildrong.iOS;
using Xamarin.Forms;
[assembly: Dependency(typeof(SoundPlayerImplementation))]
namespace Fildrong.iOS
{
    public class SoundPlayerImplementation : ISoundPlayer
    {
        // Need to have the iOS audio player scoped outside of the method so the playback can persist
        private AVAudioPlayer audioPlayer;

        public SoundPlayerImplementation()
        {

        }

        public void PlaySoundFile(string fileName)
        {
            StopSoundFile();
            //var toto = Path.GetDirectoryName(fileName) + "/" + Path.GetFileNameWithoutExtension(fileName);

            string sFilePath = NSBundle.MainBundle.PathForResource(Path.GetDirectoryName(fileName) + "/" + Path.GetFileNameWithoutExtension(fileName), "mp3");
            NSUrl url = NSUrl.FromString(sFilePath);

            audioPlayer = AVAudioPlayer.FromUrl(url);

            if (audioPlayer != null)
            {
                audioPlayer.FinishedPlaying += (object sender, AVStatusEventArgs e) =>
                {
                    audioPlayer = null;
                };
                audioPlayer.Play();
            }
        }
        public void StopSoundFile()
        {
            if (audioPlayer != null)
            {
                audioPlayer.Stop();
                audioPlayer.Dispose();
            }
        }
    }
}