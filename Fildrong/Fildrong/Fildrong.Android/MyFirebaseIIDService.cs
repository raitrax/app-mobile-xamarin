﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using Firebase.Messaging;

namespace Fildrong.Droid
{
    [Service(Name = "com.raitrax.fildrong.MyFirebaseMessagingService")]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseIIDService";
        public override void OnNewToken(string token)
        {
            base.OnNewToken(token);
            Console.WriteLine("NEW_TOKEN", token);
            SendRegistrationToServer(token);
        }
        void SendRegistrationToServer(string token)
        {
            // send this token to server
        }
    }
}