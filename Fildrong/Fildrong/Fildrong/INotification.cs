﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fildrong
{
    public interface INotification
    {
        void CreateNotification(String title, String message);
    }
}
