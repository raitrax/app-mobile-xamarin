﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fildrong.Models
{
    public class Song
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string ImageCat { get; set; }
        public string Category { get; set; }
        public string Mp3 { get; set; }
        public Uri Url { get; set; }
        public override string ToString()
        {
            return Name;
        }
        public Song()
        {
        }
    }
}
