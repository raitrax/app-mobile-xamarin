﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Fildrong.Models
{
    public class SongDatabase
    {
        private SQLiteConnection conn;

        //CREATE  
        public SongDatabase()
        {
            conn = DependencyService.Get<ISQLite>().GetConnection();
            conn.CreateTable<Song>();
        }

        //READ  
        public IEnumerable<Song> GetSong()
        {
            var songs = (from son in conn.Table<Song>() select son);
            return songs.ToList();
        }
        //INSERT  
        public string AddSong(Song song)
        {
            conn.Insert(song);
            return "success";
        }
        //DELETE  
        public string DeleteSong(int id)
        {
            conn.Delete<Song>(id);
            return "success";
        }
    }
}
