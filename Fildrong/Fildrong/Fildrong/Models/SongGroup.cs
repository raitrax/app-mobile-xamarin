﻿using System.Collections.Generic;

namespace Fildrong.Models
{
    public class SongGroupCollection : List<Song>
    {
        public string Name { get; private set; }
      
        public SongGroupCollection(string name, List<Song> song) : base(song)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
