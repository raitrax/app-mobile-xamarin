﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fildrong.Models
{
    public class Liens
    {
        public string Mp3 { get; set; }
        public Uri Url { get; set; }
    }
}
