﻿using Fildrong.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Fildrong.Services
{
    public static class DataService
    {
        #region init category
        public static string CategoryAlerte = "Alerte Live";
        public static string CategoryImitation = "Imitations";
        public static string CategoryNewStrat = "Nouvelle Strat";
        public static string CategoryMusique = "Musique";
        public static string CategoryLiveDivers = "Live Divers";

        public static string CategoryLivePokemonSaltyPlatinium = "Pokemon Salty Platinium";
        public static string CategoryLivePokemonDonjonMystereExplorateurDuCiel = "Pokemon Donjon Mystere Explorateur Du Ciel";
        public static string CategoryLivePokemonDonjonMystereDX = "Pokemon Donjon Mystere DX";
        public static string CategoryLivePokemonInfiniteFusion = "Pokemon Infinite Fusion";

        public static string CategoryLivePokemonX = "Pokemon X";
        public static string CategoryLivePokemonEpee = "Pokemon Epee";
        public static string CategoryLivePokemonPlatineFullRandom = "Pokemon Platine Full Random";
        public static string CategoryLivePokemonClover = "Pokemon Clover";
        public static string CategoryLivePokemonBlazeBlack = "Pokemon Blaze Black";
        public static string CategoryLivePokemonEclatPourpre = "Pokemon Eclat Pourpre";
        public static string CategoryLivePokemonMindCrystal = "Pokemon Mind Crystal";
        public static string CategoryLivePokemonEmeraudeFullRandom = "Pokemon Emeraude Full Random";
        public static string CategoryLivePokemonUltraSoleil = "Pokemon Ultra Soleil";
        public static string CategoryLivePokemonVega = "Pokemon Vega";
        public static string CategoryLivePokemonInsurgence = "Pokemon Insurgence";
        public static string CategoryLivePokemonUranium = "Pokemon Uranium";
        public static string CategoryLivePokemonEmpyrean = "Pokemon Empyrean";

        static string ImageNewStrat = "NS.PNG";

        static string ImagePokemonSaltyPlatinium = "Salty.jpg";
        static string ImagePokemonDonjonMystereExplorateurDuCiel = "donjonmystereciel.png";
        static string ImagePokemonDonjonMystereDX = "pdmdx.jpg";
        static string ImagePokemonInfiniteFusion = "infinite.jpg";
        static string ImagePokemonX = "pokemonx.jpg";
        static string ImagePokemonEpee = "epee.jpg";
        static string ImagePokemonPlatineFullRandom = "platinum.jpg";
        static string ImagePokemonClover = "clover.jpg";
        static string ImagePokemonBlazeBlack = "blazeblack2.png";
        static string ImagePokemonEclatPourpre = "eclatpourpre.jpg";
        static string ImagePokemonMindCrystal = "mindcrystal.png";
        static string ImagePokemonEmeraudeFullRandom = "Emeraude.jpg";
        static string ImagePokemonUltraSoleil = "ultrasoleil.jpg";
        static string ImagePokemonVega = "vega.png";
        static string ImagePokemonInsurgence = "insurgence.jpg";
        static string ImagePokemonUranium = "uranium.jpg";
        static string ImagePokemonEmpyrean = "Empyrean.png"; 
        #endregion


        public static List<Song> Listsong { get; private set; } = new List<Song>
        {
            #region Alerte live
            new Song
            {
                Name = "De type acier",
                Image = "alerte_acier.PNG",
                Category = CategoryAlerte,
                Mp3 = "alerte/typeacier.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YFOsAH7DNvu5Ajlb8BW5HUIOG2mwu7Yv")
            },
            new Song
            {
                Name = "Primo Kyogre",
                Image = "alerte_pkgr.PNG",
                Category = CategoryAlerte,
                Mp3 = "alerte/PrimoKGR.mp3",
                Url = new Uri("https://drive.google.com/open?id=1TBeaNG6rNqBDO0xOqIsoh2OL4bi1XXcs")
            },
            new Song
            {
                Name = "Terrible désillusion",
                Image = "alerte_terrible.PNG",
                Category = CategoryAlerte,
                Mp3 = "alerte/desillusion.mp3",
                Url = new Uri("https://drive.google.com/open?id=1GZE_SqgtMlw3vNWqpz9ubfqsYwUzPjKm")
            },
            new Song
            {
                Name = "Réveille toi",
                Image = "alerte_putainmerde.PNG",
                Category = CategoryAlerte,
                Mp3 = "alerte/reveiltoi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1D9exwMi78ni15FaMZ9ec9nfT9REo3tzf")
            },
            new Song
            {
                Name = "Mais c'est une boule",
                Image = "alerte_boule.PNG",
                Category = CategoryAlerte,
                Mp3 = "alerte/Boule.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VS10K555g9-pHwy6XtAAQ8RT-me4cZSC")
            },
            #endregion
            #region Imitations
            new Song
            {
                Name = "Allemand drong",
                Image = "imitation_allemandrong.jpg",
                Category = CategoryImitation,
                Mp3 = "imitation/Allemandrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mOdrPciON2c1tOHYwr7W31iWrfMdrzW-")
            },
            new Song
            {
                Name = "DJ Fildrong",
                Image = "imitation_djfildrong.PNG",
                Category = CategoryImitation,
                Mp3 = "imitation/DJFILDRONG.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rJzwVgXJuYPjZrPqFBf6Qz9WP787hT9x")
            },
            new Song
            {
                Name = "Donald drong",
                Image = "imitation_donaldrong.jpg",
                Category = CategoryImitation,
                Mp3 = "imitation/Donaldrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1helm-t22N8tXZQfT1ZNXHykmNi3EL_Un")
            },
            new Song
            {
                Name = "Fildrong Japonais",
                Image = "imitation_japon.jpg",
                Category = CategoryImitation,
                Mp3 = "imitation/FildrongJapon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1OZj9NtWr0YkLnuNM6pi3Iff-m3QDT-uR")
            },
            new Song
            {
                Name = "Fildrong sauvage apparait",
                Image = "imitation_Fifousauvage.jpg",
                Category = CategoryImitation,
                Mp3 = "imitation/FildrongSauvage.mp3",
                Url = new Uri("https://drive.google.com/open?id=1FGHIVLf2BthKdESMLKvNzaYIT3OInUns")
            },
            new Song
            {
                Name = "Tari drong",
                Image = "imitation_taridrong.jpg",
                Category = CategoryImitation,
                Mp3 = "imitation/Taridrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=15YWQI5lSyk7iqhBSLpSI1X2Cr_2-VlkI")
            },
            new Song
            {
                Name = "Tutanka drong",
                Image = "imitation_tutankadrong.jpg",
                Category = CategoryImitation,
                Mp3 = "imitation/Tutankadrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lrj6-eHkStzkTZSuA80VlbmVpIY84_6g")
            },
            
            #endregion
            #region Nouvelle strat
            new Song
            {
                Name = "6 tours de sommeil sur 7",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/6tours.mp3",
                Url = new Uri("https://drive.google.com/open?id=1i51Ttj4sW6JYd46PCE0MzC9I8Pzj9LEr")
            },
            new Song
            {
                Name = "Brasegali Picpic",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/brasegali_picpic.mp3",
                Url = new Uri("https://drive.google.com/open?id=13P0LaHJjC6n36dB62uHZ19Zk4Cm4mykB")
            },
            new Song
            {
                Name = "Chinois rond",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/chinoisrond.mp3",
                Url = new Uri("https://drive.google.com/open?id=1x-zuoi704yHR79pTnX-a1_cFYdRAuGvX")
            },
            new Song
            {
                Name = "Croc Feu",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/CrocFeuv2.mp3",
                Url = new Uri("https://drive.google.com/open?id=1bxQdVe6RdgnIAKkmjk4fMKNf3iwtm4Pu")
            },
            new Song
            {
                Name = "Dimoret",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/dimoret.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VXAGrVcVYTbNTBkQdDR0Ck3in9JlpY4K")
            },
            new Song
            {
                Name = "Il a peur de Zarbi",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/il_a_peur_de_zarbi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uce2_U1D9yEr7XZDeoiqWaJF38VOFSTQ")
            },
            new Song
            {
                Name = "J'ai perdu ma bite",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/jai_perdu_ma_bite.mp3",
                Url = new Uri("https://drive.google.com/open?id=15q3QXPCVFQQ0y2cB88v8Y5EeBd8f-6F8")
            },
            new Song
            {
                Name = "Je remonte ma braguette",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Braguette.mp3",
                Url = new Uri("https://drive.google.com/open?id=1JlUEPjEqmPgcx4acWA9ymz2YwJUb1PiS")
            },
            new Song
            {
                Name = "Je suis entrain de gagner",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/gagner.mp3",
                Url = new Uri("https://drive.google.com/open?id=1n8HO-IpqIolQE1g3I02QDz4_qQIQeV3F")
            },
            new Song
            {
                Name = "Katagami",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/katagami.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ld0UPX8M9J4EtWrzNK2CHbGIhCe8iJOC")
            },
            new Song
            {
                Name = "Leuphorie Liliput",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/leuphorie_liliput_pute_putain.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ru2qcY0t27LhP8xwkJRbKk2a_08TGjbb")
            },
            new Song
            {
                Name = "Leveinard Lance-soleil",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/leveinard_lance_soleil.mp3",
                Url = new Uri("https://drive.google.com/open?id=19nr1gVHZzWIFSDQTXJrARyalTH6LjlT0")
            },
            new Song
            {
                Name = "Melodelfe Double-Kill",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/melodelfe_double_kill.mp3",
                Url = new Uri("https://drive.google.com/open?id=117Aye86bQXGY5-TKxG6V4g8n68aB1WHr")
            },
            new Song
            {
                Name = "Mew Distorsion",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/mew_disto.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wTZL7GqX4OAV_R2lUmBUN6vjFYAP15Bx")
            },
            new Song
            {
                Name = "Non Non NON",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/nonnonnon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VrrqxHOEs8RBRGzlHE2vWz-FuIF5Jj5i")
            },
            new Song
            {
                Name = "Pas de réponse du Président",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/pas_de_reponse_du_president.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vfEhQ4RxXeFuQYp49VoNlcPQL2n-XrZQ")
            },
            new Song
            {
                Name = "Poudre Toxic sur Scorvol",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/poudre_toxic_sur_scorvol.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mzFS8A8ElMHa2-HBTmucsp_oUdDEL8rU")
            },
            new Song
            {
                Name = "Primo Groudon Eclat-Rock",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/primo_groudon_eclat-rock.mp3",
                Url = new Uri("https://drive.google.com/open?id=1luH3zXG9dKdRIXMGpiA66g8QjL2jPOIS")
            },
            new Song
            {
                Name = "Pyrax Orbe Vie Vol-vie",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/pyrax_orbe_vie_vol_vie.mp3",
                Url = new Uri("https://drive.google.com/open?id=1QHxQmFrFfTYEs1h09r6qDUTVDQSXel0y")
            },
             new Song
            {
                Name = "Boule de neige sur darumacho de Galar",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Boule_de_neige_sur_darumacho_de_galar.mp3",
                Url = new Uri("https://drive.google.com/file/d/1gZSaKj0a4Btl5WrA1SDZ2PHSnDIvm2lM/view?usp=sharing")
            },
            new Song
            {
                Name = "Eh non vous n'aurez pas ma chaine Youtube",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Eh_non_vous_naurez_pas_ma_chaine_youtube.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ehQkhNnFoWqb_JkOEaogan_wnyt8zvqt/view?usp=sharing")
            },
            new Song
            {
                Name = "Jamy est une grosse merde en stategie Pokemon",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Jamy_est_une_grosse_merde_en_stategie_pokemon.mp3",
                Url = new Uri("https://drive.google.com/file/d/163f0xbIlBkUMPCpdgATs5faYuKHoWVmL/view?usp=sharing")
            },
            new Song
            {
                Name = "Jamy nous raconte comment on a des enfants",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Jamy_nous_raconte_comment_on_a_des_enfants.mp3",
                Url = new Uri("https://drive.google.com/file/d/1cZ4WrnRkQSWDNntbZ2ZWnkbpHKXYm3lK/view?usp=sharing")
            },
            new Song
            {
                Name = "Jamy tu pues",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Jamy_tu_pues.mp3",
                Url = new Uri("https://drive.google.com/file/d/1OTy5LNu7meQUs3b5P3lRrFrnLI5QO_si/view?usp=sharing")
            },
            new Song
            {
                Name = "La ceinture force",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/La_ceinture_force.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ygXEKKAfkldVWduZS1TmrDjgED9SrTZz/view?usp=sharing")
            },
            new Song
            {
                Name = "Mais dis donc jamy est-ce un match de merde?",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/Mais_dis-donc_jamy_est-ce_un_match_de_merde.mp3",
                Url = new Uri("https://drive.google.com/file/d/1-4zepc-Krh4Uw5ZnfR2eNWqd1xxzFbRN/view?usp=sharing")
            },
            new Song
            {
                Name = "La vive griffe blizzard",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/la_vive_griffe_blizard.mp3",
                Url = new Uri("https://drive.google.com/file/d/1eiu1PuUZeNnSLUQ8LKWwTv2nW8tpDewK/view?usp=sharing")
            },
            new Song
            {
                Name = "Tocopisco drong",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/tocopisco_drong.mp3",
                Url = new Uri("https://drive.google.com/file/d/10ywIiSAaDzIxznKHxb1Ajk5ViN9aO0Hr/view?usp=sharing")
            },
            new Song
            {
                Name = "Tadmorv drong",
                Image = ImageNewStrat,
                Category = CategoryNewStrat,
                Mp3 = "nouvellestrat/tadmorv_drong.mp3",
                Url = new Uri("https://drive.google.com/file/d/1AqapQ3htLS0VzNFzFkkaB3AMdJEJRIdq/view?usp=sharing")
            },

            #endregion
            #region Divers
            new Song
            {
                Name = "Mais c'est du génie",
                Image = "live_genie.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/genie.mp3",
                Url = new Uri("https://drive.google.com/open?id=1P_zzwkiTeyn3RIrNTsw5rah-FLRUnyAY")
            },
            new Song
            {
                Name = "Parti rager",
                Image = "live_purrage.jpg",
                Category = "Lives/Let's Play",
                Mp3 = "live/ragedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1QOn9aJhHKPFfHf3uBpa0d7aE2rofnWZu")
            },
            new Song
            {
                Name = "Pompier",
                Image = "live_pompier.jpg",
                Category = "Lives/Let's Play",
                Mp3 = "live/Pompier.mp3",
                Url = new Uri("https://drive.google.com/open?id=1iiBSZpOqWHqG1kDhDEfEZiiL0AEhVSNZ")
            },
            new Song
            {
                Name = "Stall",
                Image = "live_pasgentil.jpg",
                Category = "Lives/Let's Play",
                Mp3 = "live/stall.mp3",
                Url = new Uri("https://drive.google.com/open?id=193xF_f0c642HKP--3TXTU2MP8RjM3Q2J")
            },
            new Song
            {
                Name = "Un trou du cul en feu",
                Image = "tierlist.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/un_trou_du_cul_en_feu.mp3",
                Url = new Uri("https://drive.google.com/file/d/1MIGV-RgLpOcuF9ccz1qlNG4utz1Y8m3S/view?usp=sharing")
            },
            new Song
            {
                Name = "Le fion sa sent pas bon",
                Image = "tierlist.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/le_fion_sa_sent_pas_bon.mp3",
                Url = new Uri("https://drive.google.com/file/d/1XEAcMwd9WjkRkDHLap8Mo10WG8uLyddW/view?usp=sharing")
            },

            new Song
            {
                Name = "Oh les boobs",
                Image = "live_genie.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/oh_les_boobs.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Mlt9p79YyDsoYGmmImXc0gxsCiWs8Dyq/view?usp=sharing")
            },

            new Song
            {
                Name = "La préparation des vidéos de Fildrong",
                Image = "live_genie.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/la_preparation_des_videos_de_fildrong.mp3",
                Url = new Uri("https://drive.google.com/file/d/1k4sPNHWd7jZU6YKQQw2VdjV-QXyYOUkb/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildrong et Among-Us",
                Image = "live_genie.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/fildrong_et_among-us.mp3",
                Url = new Uri("https://drive.google.com/file/d/1yJOSOOa3yOJYm-ycxF3AmGZyhhDHoX7V/view?usp=sharing")
            },
            new Song
            {
                Name = "Robotdrong",
                Image = "live_genie.png",
                Category = "Lives/Let's Play",
                Mp3 = "live/robotdrong.mp3",
                Url = new Uri("https://drive.google.com/file/d/1b9w0laOjzUE5fm3vjZXJpVbzJxQHzd_V/view?usp=sharing")
            },

            #endregion
            #region Live Pokemon Salty Platinium
            new Song
            {
                Name = "Cheh",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/Cheh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rN67c6WvdHA2_L6YIRbmAaWTbLKzkzwP")
            },
            new Song
            {
                Name = "Ernestine",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/Ernestine.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MCttnLgq0bIs5wN1OrGsjw8mmaWbZZjK")
            },

            new Song
            {
                Name = "Banquodrong CHEH",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/banquodrong_cheh.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ohTB4uYDYn4CZwHOLAuXJnZtB0Gz2-b4/view?usp=sharing")
            },
            new Song
            {
                Name = "Carnarequet a changé",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/carnarequet_a_change.mp3",
                Url = new Uri("https://drive.google.com/file/d/1z9CBhC-srnmId2XX3xuSP0u2i0DR3VMK/view?usp=sharing")
            },
            new Song
            {
                Name = "Dieu Bitenfesse",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/dieu_bitenfesse.mp3",
                Url = new Uri("https://drive.google.com/file/d/18kOC9dAmhicvl2pn9EszMpccDGSoFGXQ/view?usp=sharing")
            },
            new Song
            {
                Name = "En train de vibre",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/en_train_de_vibre.mp3",
                Url = new Uri("https://drive.google.com/file/d/1eD0X8Xb5cXof6cnnMJTgF968vfx1vXvy/view?usp=sharing")
            },
            new Song
            {
                Name = "Eye of the Fildrong",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/eye_of_the_fildrong.mp3",
                Url = new Uri("https://drive.google.com/file/d/17ZqrnXH328TokosgRwGw0tebWLRfcexj/view?usp=sharing")
            },
            new Song
            {
                Name = "Faut qu'on mette plumdanlcul",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/faut_quon_mette_plumdanlcul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1tWABFMpS5KzM23RZgOMSZw_2e8bZ-A4W/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildrong du sud",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/fildrong_du_sud.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Mv4a8k_Iuov0gCsfbuxDTdIOM1Etwet-/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildronglevrai sur Instagram",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/fildronglevrai_sur_instagram.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ZdCr8-KkaKnOOA_9DvUVQ7JNI7SueCCL/view?usp=sharing")
            },
            new Song
            {
                Name = "Fou rire",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/fou_rire.mp3",
                Url = new Uri("https://drive.google.com/file/d/1lXYCDY-AQ_qaGnCBwoiPd7Ty2yzH3wZK/view?usp=sharing")
            },
            new Song
            {
                Name = "Hate de te péter le cul",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/hate_de_te_peter_le_cul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1UVbnvpErnMnWHuJFG52NfSH-pY4SbTyp/view?usp=sharing")
            },
            new Song
            {
                Name = "Hommage",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/Hommage.mp3",
                Url = new Uri("https://drive.google.com/file/d/195v_W5PkT_1yCLTjtfQXFeB5TtmF-fbY/view?usp=sharing")
            },
            new Song
            {
                Name = "Il est malin",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/il_est_malin.mp3",
                Url = new Uri("https://drive.google.com/file/d/1CndGIj-AAa2gR4nMNLRHXSD3EkZvWeqn/view?usp=sharing")
            },
            new Song
            {
                Name = "Je dois déjà réfléchir",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/je_dois_deja_reflechir.mp3",
                Url = new Uri("https://drive.google.com/file/d/1fHGl6L2qTiaSQmmM5xYxO1VqiD6lDTCN/view?usp=sharing")
            },
            new Song
            {
                Name = "John salami",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/john_salami.mp3",
                Url = new Uri("https://drive.google.com/file/d/1SbmM-Fnu9tcfKaJh_xD5MlbOQnZMhHeh/view?usp=sharing")
            },
            new Song
            {
                Name = "Le coffret mode",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/le_coffret_mode.mp3",
                Url = new Uri("https://drive.google.com/file/d/1IQsAWuSLyHTa6fQOSBejzqY1i1XnaBdD/view?usp=sharing")
            },
            new Song
            {
                Name = "Médou",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/medou.mp3",
                Url = new Uri("https://drive.google.com/file/d/1rbeJWaVY8K5ev4-jpfxN7tevg0Awo-BY/view?usp=sharing")
            },
            new Song
            {
                Name = "Oh putaing",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/oh_putaing.mp3",
                Url = new Uri("https://drive.google.com/file/d/1-PUoxx9AFzWwvoACDRaVYSBoIH7B644k/view?usp=sharing")
            },
            new Song
            {
                Name = "Pas de bite",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/pas_de_bite.mp3",
                Url = new Uri("https://drive.google.com/file/d/1JPUN_CykkHaurCW9K7rjatjrmOlfWvIM/view?usp=sharing")
            },
            new Song
            {
                Name = "Petits coeurs",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/petits_coeurs.mp3",
                Url = new Uri("https://drive.google.com/file/d/1QGAsOBXuTsY0mGGQ_fNU2sqtEopsAfpc/view?usp=sharing")
            },
            new Song
            {
                Name = "Rage",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/rage_1.mp3",
                Url = new Uri("https://drive.google.com/file/d/1MoBcooUPoc75K9-SISCRTEJ2ldnShrEo/view?usp=sharing")
            },
            new Song
            {
                Name = "Switch dans le cul",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/switch_dansle_cul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1zxKWVjHJpb07WwyxAIkkhRgNS9TyWtQQ/view?usp=sharing")
            },


            new Song
            {
                Name = "La malédiction des fausses bites",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/la_malediction_des_fausses_bites.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Upy1m8tjasUhDAq9rizb9JoWLAQopXaS/view?usp=sharing")
            },
            new Song
            {
                Name = "Badges",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/badge.mp3",
                Url = new Uri("https://drive.google.com/file/d/1EMfjvBGwa5zEBcq8CD2gatidc5GSnnEO/view?usp=sharing")
            },
            new Song
            {
                Name = "A chaque fois",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/a_chaque_fois.mp3",
                Url = new Uri("https://drive.google.com/file/d/11wpV8N3PxSzPfx3J0g2Tyw4h-8MsbprI/view?usp=sharing")
            },
            new Song
            {
                Name = "Joli canapet",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/joli_canapet.mp3",
                Url = new Uri("https://drive.google.com/file/d/1l1qGR6RVwUG7KoM2Z7PWswDo_L2ZOCKV/view?usp=sharing")
            },
            new Song
            {
                Name = "Jvoulais juste briller V2",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/jvoulais_juste_briller_ver2.mp3",
                Url = new Uri("https://drive.google.com/file/d/1wnG-2QuYbBz2X1pufSAEZvwTVAoaUSaj/view?usp=sharing")
            },
            new Song
            {
                Name = "Mignon",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/mignon.mp3",
                Url = new Uri("https://drive.google.com/file/d/1gdopgFyxd-mMpO72jD6qcYadsEKJC1ux/view?usp=sharing")
            },
            new Song
            {
                Name = "Poing glace",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/poing_glace.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Xz_33yWOXAZjl0UJ1VLIW0zGSFV6CRU5/view?usp=sharing")
            },
            new Song
            {
                Name = "Explosion mentale",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/explosion_mentale.mp3",
                Url = new Uri("https://drive.google.com/file/d/1NnrRcbmLM77rtqC2PO9OtZWNONrMo21w/view?usp=sharing")
            },
            new Song
            {
                Name = "Jvoulais juste briller",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/jvoulais_juste_briller.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Mnuavuaat0XolEF-225_HdzV9TYEHxPW/view?usp=sharing")
            },
            new Song
            {
                Name = "Le mec qui a dit canon graine",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/le_mec_qui_a_dit_canon_graine.mp3",
                Url = new Uri("https://drive.google.com/file/d/11bPU6M4VNY6Fvlnx8HacYrCUumCHXRIE/view?usp=sharing")
            },
            new Song
            {
                Name = "Heureusement",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/heureusement.mp3",
                Url = new Uri("https://drive.google.com/file/d/1eoG70tprmyaVhgh78phGpiCGiJhwJ3a0/view?usp=sharing")
            },
            new Song
            {
                Name = "Caca qui pue la mort",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/caca_qui_pue_la_mort.mp3",
                Url = new Uri("https://drive.google.com/file/d/1-uuYCUDCrVtAL18bbZUhrPsEmAAfEFCh/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildrong russe",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/fildrong_russe.mp3",
                Url = new Uri("https://drive.google.com/file/d/1L_4T6X1La4YTTwJcKl5mYGF2GioYhQNG/view?usp=sharing")
            },
            new Song
            {
                Name = "Laves tes badges",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/laves_tes_badges.mp3",
                Url = new Uri("https://drive.google.com/file/d/1_KA7K4sqylhBjhze-NdADknWfzJ8mbSv/view?usp=sharing")
            },
            new Song
            {
                Name = "Le batard il est trop con",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/le_batard_il_est_trop_con.mp3",
                Url = new Uri("https://drive.google.com/file/d/1c6WWcyQi8-Wd92DtJduewqg5IRl68Gh3/view?usp=sharing")
            },
            new Song
            {
                Name = "Nettoyer mes toilettes",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/nettoyer_mes_toilettes.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Z3q6TR31zrStUSFMn1hyA7wHuTVFm6VF/view?usp=sharing")
            },
            new Song
            {
                Name = "Pisser du cul",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/pisser_du_cul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1m-USZKTn4SxfPkWhh0B18sE1Flz2I7XP/view?usp=sharing")
            },
            new Song
            {
                Name = "Plaqué au sol",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/Plaque_au_sol.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Ts-JXehrCh2r0ozX-3KQY96k6LIrFiWd/view?usp=sharing")
            },
            new Song
            {
                Name = "T'astiques bien",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/tastiques_bien.mp3",
                Url = new Uri("https://drive.google.com/file/d/1vEC4mz1gU7kgTSvHrfc-BlOteErnjVTp/view?usp=sharing")
            },
            new Song
            {
                Name = "Fini en a",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/fini_en_a.mp3",
                Url = new Uri("https://drive.google.com/file/d/1hfbCxffhpdyBJi3CiN-8u-Kdz0vvJadQ/view?usp=sharing")
            },
            new Song
            {
                Name = "Qui dit mort",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/qui_dit_mort.mp3",
                Url = new Uri("https://drive.google.com/file/d/1IkgFqw6zuqWoHgb1uZEOLxjTkGbVW8IS/view?usp=sharing")
            },
            new Song
            {
                Name = "Surf pour surfer",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/surf_pour_surfer.mp3",
                Url = new Uri("https://drive.google.com/file/d/140rzoLMj55mDPvuNxL21Hji3tu5Dmxuc/view?usp=sharing")
            },
            new Song
            {
                Name = "Rester calme",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/rester_calme.mp3",
                Url = new Uri("https://drive.google.com/file/d/14gCmeJraPnpZ18ffapn0l0GFMiI6XRx8/view?usp=sharing")
            },
            new Song
            {
                Name = "Bitenfesse mort",
                Image = ImagePokemonSaltyPlatinium,
                Category = CategoryLivePokemonSaltyPlatinium,
                Mp3 = "Salty/bitenfesse_mort.mp3",
                Url = new Uri("https://drive.google.com/file/d/1IiSkxYRWXuQSH1K9HBoTKOya4hW-fZQU/view?usp=sharing")
            },


            #endregion
            #region Live Pokemon Donjon Mystère Explorateur du ciel FAIT
            new Song
            {
                Name = "Asmedrong",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/asmedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1NI8P3UmqNkt6BK3p4WPJUrbiwM0WkJ6u")
            },
            new Song
            {
                Name = "C'est la fête dans mon slip",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/cest_la_fete_dans_mon_slip.mp3",
                Url = new Uri("https://drive.google.com/open?id=1RL4aw4h1jFBgVa0z-eXUGbC7p1XzJERe")
            },
            new Song
            {
                Name = "C'était mieux quand j'etais mort",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/cetait_mieux_quand_jetais_mort.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ydJlqIdtCFfl4zz9AR0jN2LZlvFbH7rG")
            },
            new Song
            {
                Name = "Choupidrong",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/choupidrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IyXBrqPNB9fsb0sU7y5eAMP6p8l3oLx6")
            },
            new Song
            {
                Name = "COMME LE TITRE DU JEU",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/COMME_LE_TITRE_DU_JEU.mp3",
                Url = new Uri("https://drive.google.com/open?id=1zxKSbUjfksJvyRPvmT6nHzASN9dTgPm_")
            },
            new Song
            {
                Name = "Dans la chatte à vos mères",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/dans_la_chatte_a_vos_meres.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ciAZ47QDTDjyAGOAbFVoAYg25KLf7D9U")
            },
            new Song
            {
                Name = "Deux en france",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/deux_en_france.mp3",
                Url = new Uri("https://drive.google.com/open?id=113wCU0xk7HYfIZZviKQf-BiD5F83hCpg")
            },
            new Song
            {
                Name = "Eh chien",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/chien.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SSsWTNF6Vt9AU4HNMg6kbuEZBH2XuRTX")
            },
            new Song
            {
                Name = "Grodoudou",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/grodoudou.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hXDPZIqy2H3vRXbW77HwR-ChsvpFDoTh")
            },
            new Song
            {
                Name = "J'ai pas de penis pour me branler",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/jai_pas_de_penis_pour_me_branler.mp3",
                Url = new Uri("https://drive.google.com/open?id=1_LBLZ5IJsUJfCDaqwk16RA-RwFfajSqC")
            },
            new Song
            {
                Name = "Jte boloss",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/jte_boloss.mp3",
                Url = new Uri("https://drive.google.com/open?id=10Yw7D5bdBQTvkl2VXC4gs4_sKVrydnxB")
            },
            new Song
            {
                Name = "Jte defonce",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/jte_defonce.mp3",
                Url = new Uri("https://drive.google.com/open?id=1awC795_0zxw0LhG-Ae8FofpoFxUCqBdU")
            },
            new Song
            {
                Name = "La diarrhée qui arrive",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/la_diarrhee_qui_arrive.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xDDEWY3q-6en4V0Yht3mJNJiVzeEupV8")
            },
            new Song
            {
                Name = "Laisse moi passer",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/laisse_moi_passer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wncYxVvwgvU-XoB21ChT5NyQFwdQbCzz")
            },
            new Song
            {
                Name = "Le TEMPS",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/le_TEMPS.mp3",
                Url = new Uri("https://drive.google.com/open?id=17MqTOXAF27ic3AQ5KZTdnGzDZ-gkK4BD")
            },
            new Song
            {
                Name = "Mais quel victime",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/mais_quel_victime.mp3",
                Url = new Uri("https://drive.google.com/open?id=1o8MI_saTMPVy_DcVHtpdp7_A3-iD74wx")
            },
            new Song
            {
                Name = "Manger la bouche pleine",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/manger_la_bouche_pleine.mp3",
                Url = new Uri("https://drive.google.com/open?id=1kfXgLfEhJRZxrAne5ujIdWFJ6xclBSco")
            },
            new Song
            {
                Name = "Massacre",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/massacre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1HWwDPSWWF-GZgpefgFceA-6KAxKRro8p")
            },
            new Song
            {
                Name = "Pète moi le cul",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/pete_moi_le_cul.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hjS3CSeoilJILhYqVs3-tKuLy--rCV58")
            },
            new Song
            {
                Name = "Portes tes burnasses",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/portes_tes_burnasses.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KB29DkHuIZa3Qlh1e7HMJCr5TKXJHHFB")
            },
            new Song
            {
                Name = "Pourquoi tu me renifles le cul",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/pourquoi_tu_me_renifles_le_cul.mp3",
                Url = new Uri("https://drive.google.com/open?id=1FlfvJKJwfdl7iQsdkmrlHzO_twivgnIO")
            },
            new Song
            {
                Name = "Refais le cul",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/refais_le_cul.mp3",
                Url = new Uri("https://drive.google.com/open?id=1EacNY12hLfBsWtSaH61LPZbFFmrJhPFL")
            },
            new Song
            {
                Name = "Rire",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/rire.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Dl09kCEVA4ffpsqk6KpwG1jiGeuMHscA")
            },
            new Song
            {
                Name = "Slip Troué",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/sliptrouer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jKDPd_YAgjineIZujSTho2R9Um_H5AnV")
            },
            new Song
            {
                Name = "Suce pet",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/suce_pet.mp3",
                Url = new Uri("https://drive.google.com/open?id=1QhE80KViHvVQEBteYXQjUGaSz-BMAwlZ")
            },
            new Song
            {
                Name = "Ta gueule",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/ta_gueule.mp3",
                Url = new Uri("https://drive.google.com/open?id=1znxXTxs4qkCsZPk1PvQUJuKVf8WoV1kJ")
            },
            new Song
            {
                Name = "The grudge 2.0",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/the_grudge_2.0.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Kg_CHdycNscxn-m3jll8k_VhFSjIKZy3")
            },
            new Song
            {
                Name = "Une corne une bite",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/une_corne_une_bite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1PzWhJTcLJz-wF7lmn7lfGowcc7lPa4Io")
            },
            new Song
            {
                Name = "Une voiture de police dans le cul",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/une_voiture_de_police_dans_le_cul.mp3",
                Url = new Uri("https://drive.google.com/open?id=1w0E1rq4aIvKAo6WpPF6qGgIIji4AzymK")
            },
            new Song
            {
                Name = "Vodka",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/Vodka.mp3",
                Url = new Uri("https://drive.google.com/open?id=1t4Gv52eOYUpf0b6T9V8zkTZuWZPOdPv3")
            },
            new Song
            {
                Name = "Vol de 90% des revenus",
                Image = ImagePokemonDonjonMystereExplorateurDuCiel,
                Category = CategoryLivePokemonDonjonMystereExplorateurDuCiel,
                Mp3 = "DonjonMystereExplorateurDuCiel/vol_de_90_des_revenus.mp3",
                Url = new Uri("https://drive.google.com/open?id=1OlwjCtvBnH6YCJkldfSHFM213hsCgp2t")
            },
            #endregion
            #region Live Pokemon Donjon Mystère DX
            #endregion
            #region Live Pokemon infinite fusion FAIT
            new Song
            {
                Name = "43",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/43.mp3",
                Url = new Uri("https://drive.google.com/open?id=13RQXer96KJY2HPs2qhpRX3qk3q2Y8q91")
            },
            new Song
            {
                Name = "Cacorbe",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/cacorbe.mp3",
                Url = new Uri("https://drive.google.com/open?id=1NW2TFMKL5LjTkpQ-xuwBF_n6E4qmEi4j")
            },
            new Song
            {
                Name = "Clefgma",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/clefgma.mp3",
                Url = new Uri("https://drive.google.com/open?id=1k0MARlRLSaeqINMf4Nyvi8y6PR5dAYZt")
            },
            new Song
            {
                Name = "Clefuck",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/clefuck.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Z2zE1xPGl7Qrn8gfjT0_9lPbPIAwSW5i")
            },
            new Song
            {
                Name = "Dududludrong",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/dududludrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vMu86zgsMcE6ZUrh-0A-PNASLyUAS87Y")
            },
            new Song
            {
                Name = "Exelangue",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/exelangue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1EUMZbjaERiPtk3U6C720OiOqvFED7bk2")
            },
            new Song
            {
                Name = "Floubite",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/floubite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Tw_m3BNP7ebyz1GJKnqFjVVbW8l-5u4I")
            },
            new Song
            {
                Name = "Gangbang par des enfants",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/gangbang_par_des_enfants.mp3",
                Url = new Uri("https://drive.google.com/open?id=1bnSzv4l8FLON81W0wppwc9unOfx9bhCb")
            },
            new Song
            {
                Name = "Google tradrong",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/google_tradrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1_P3Sj-Km9sj2icOCWfzcYU_0GIXJwFS4")
            },
            new Song
            {
                Name = "Je n'ai pas le choix je me torche avec les doigts",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/je_nai_pas_le_choix_je_me_torche_avec_les_doigts.mp3",
                Url = new Uri("https://drive.google.com/open?id=12IFiVLWKP3m7UV52q7TkrNX8zvDNzBCU")
            },
            new Song
            {
                Name = "La prostitution",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/la_prostitution.mp3",
                Url = new Uri("https://drive.google.com/open?id=1QLdz7Lqm5680pjRdcfQhtL_X7KDqMURE")
            },
            new Song
            {
                Name = "Manix",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/manix.mp3",
                Url = new Uri("https://drive.google.com/open?id=1yVo1Vu9dciM7HefmZlnbFv8hXMQlqUKI")
            },
            new Song
            {
                Name = "Prout",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/prout.mp3",
                Url = new Uri("https://drive.google.com/open?id=1nJ9YDOlESUbpadcZJp3-2PDzQmhLEmlf")
            },
            new Song
            {
                Name = "Un bon pour un belo",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/un_bon_pour_un_belo.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uHh__70pCHPK_oyRFhludMQ4fGzcayiW")
            },
            new Song
            {
                Name = "Un boudin acheté, deux offerts",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/un_boudin_achete_deux_offerts.mp3",
                Url = new Uri("https://drive.google.com/open?id=1FeJ7hUqfIQh1H7-qAfk39kUbhs-Qvybi")
            },
            new Song
            {
                Name = "Zbrzbrzbrr",
                Image = ImagePokemonInfiniteFusion,
                Category = CategoryLivePokemonInfiniteFusion,
                Mp3 = "InfiniteFusion/zbrzbrzbrr.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tlCrtnrsGs6XA08ZIQR81rIpe_tgJ3P4")
            },
            #endregion
            #region Live Pokemon X FAIT
            new Song
            {
                Name = "Aire de feu",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/aire_de_feu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1A8Zw8A8BcCDBnhzSTwOJ1CLR4WvF58LV")
            },
            new Song
            {
                Name = "As-tu cherché ?",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/as_tu_cherche_dans_la_chatte_a_ta_mere_mon_cher.mp3",
                Url = new Uri("https://drive.google.com/open?id=1S2n-erBNIpwEUDbp_CRyqNQIPjcuOZad")
            },
            new Song
            {
                Name = "Bat toi contre moi salope",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/bat_toi_contre_moi_salope.mp3",
                Url = new Uri("https://drive.google.com/open?id=1-MwTz9PukyO2hzlkB-Ga2tJOmtxihmhM")
            },
            new Song
            {
                Name = "Ça va tuer",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/ca_va_tuer.mp3",
                Url = new Uri("https://drive.google.com/open?id=10Sjtie1iYYKZWZGv5m7H3kxg683d29tR")
            },
            new Song
            {
                Name = "CHEHDRONG",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/CHEHDRONG.mp3",
                Url = new Uri("https://drive.google.com/open?id=1UuEEvxt7K0kXKby_lRRIUdmAfhlWefm0")
            },
            new Song
            {
                Name = "Coupure de Coran",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/CoupureDeCoran.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IClVMvXjGhjg6friCYizp2oMVSyOHeMr")
            },
            new Song
            {
                Name = "Fildrong le malin",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/fildrong_le_malin.mp3",
                Url = new Uri("https://drive.google.com/open?id=1V66HDJAj149-62CJpS0uzfGV9L8RoLkc")
            },
            new Song
            {
                Name = "Hipopotasdrong",
                Image = ImagePokemonX,
                Category = CategoryImitation,
                Mp3 = "X/hipopotasdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1TxtcXEwEEBdipmiZeOHRoc32hwkDFWow")
            },
            new Song
            {
                Name = "Goupelindrong",
                Image = ImagePokemonX,
                Category = CategoryImitation,
                Mp3 = "X/goupelindrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Ps1-11VezSEikkJwhLf2Z9hs5zOdVtkz")
            },
            new Song
            {
                Name = "Il est trop mignon",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/il_est_trop_mignon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wS2_q1tk5Uf5iUP0YoEUw_b0Gmin3yH6")
            },
            new Song
            {
                Name = "J'ai déjà capturé un pokemon",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/j_ai_deja_captuer_un_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ceDfuEQ69JJ0BCIMK1rnadWcU2BV2Tta")
            },
            new Song
            {
                Name = "J'aime manger des raviolis",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/j_aime_manger_des_raviolis.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jHJvpAGbv4Cr16Ya1FvGm1pA6RMJ5_B3")
            },
            new Song
            {
                Name = "Je m'habille pour la journée",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/je_m_habille_pour_la_journee.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YqLZ-xh2l9S2vWr1mkIeWYcazJW9SXB3")
            },
            new Song
            {
                Name = "Je m'identifie comme un méga gardevoir shiny",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/je_m_identifie_comme_un_mega_gardevoir_shiny.mp3",
                Url = new Uri("https://drive.google.com/open?id=18nOKBw5tVxRQZL2Gk9OyXSuJ7oj-T5P9")
            },
            new Song
            {
                Name = "Je me retiens sur les blagues",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/je_me_retiens_sur_les_blagues.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Lh9n35OM2d4mlo2U41AfzqzpByCmws1K")
            },
            new Song
            {
                Name = "Je m'appelle Fildrong",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/je_m_appelle_fildrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Jvypx8sdIzItu4bVgNNs2bIsuykr-8bq")
            },
            new Song
            {
                Name = "Jean kevin la duchesse",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/jean_kevin_la_duchesse.mp3",
                Url = new Uri("https://drive.google.com/open?id=164cSDByiNv6BSnhVn6-0aO0b4P95pJ4B")
            },
            new Song
            {
                Name = "Jean moucha kinesithérapeute",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/jean_moucha_kinesitherapeute.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ggCGHSIBKGHkwtpGJ45ealTsF9nLoT4j")
            },
            new Song
            {
                Name = "Jsuis trop con",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/jsuis_trop_con.mp3",
                Url = new Uri("https://drive.google.com/open?id=1CG1OTjgb3QhZdynk4XilUVq_D5wz6w6W")
            },
            new Song
            {
                Name = "Il m'a vu dans le miroir",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/il_m_a_vu_dans_le_miroir.mp3",
                Url = new Uri("https://drive.google.com/open?id=1zaz8WoRbjtX0pA5lVIkIl4bgaYAcqokV")
            },
            new Song
            {
                Name = "Lucario est sympa",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/lucario_est_sympa.mp3",
                Url = new Uri("https://drive.google.com/open?id=1zKJRPQ7QXD6SkudDlDdqN6YFrU-_U2TR")
            },
            new Song
            {
                Name = "Mais biten",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/mais_biten.mp3",
                Url = new Uri("https://drive.google.com/open?id=1T6tnw3_lZVoqp3ivlZ-O0xJukQhfR4Ny")
            },
            new Song
            {
                Name = "Met les sprites direct",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/met_les_sprites_direct.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Aep8zSwr7nLjYbzgHxBNQSPg0No5xseW")
            },
            new Song
            {
                Name = "Mongol victime",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/mongol_victime.mp3",
                Url = new Uri("https://drive.google.com/open?id=1t5QiU1ACy8apR-ML1lNps0hO3kchImba")
            },
            new Song
            {
                Name = "Oh c'est trop mignon",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/oh_cest_trop_mignon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ku109gppqh6QHfrdx1o2lqJEFO7QWUb9")
            },
            new Song
            {
                Name = "Pas de freeze",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/pas_de_freeze.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KBHxtEG08GjRw3MPHcrdlYLesciG8rso")
            },
            new Song
            {
                Name = "Retardrong",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/retardrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=17GSmTZ4Q6xI9apM36kpZibi2mEwX_x1j")
            },
            new Song
            {
                Name = "WHAT",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/WHAT.mp3",
                Url = new Uri("https://drive.google.com/open?id=1x7PQj3LuHAqy6AiICQGjMr1itd8rbn6p")
            },
            new Song
            {
                Name = "Mais jsuis trop con",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Mais_chui_trop_con_mais_ptn_ca_menerve.mp3",
                Url = new Uri("https://drive.google.com/open?id=1OwkBtmPLUrMwf9Aym7sTGzBvOLFJ0HbQ")
            },
            new Song
            {
                Name = "Mongolmar",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Mongolmar.mp3",
                Url = new Uri("https://drive.google.com/open?id=1a9MS_GHwJ4ZfK6x0kKeixu8MFdBTyeZh")
            },
            new Song
            {
                Name = "Oohoohoohooh",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/oohoohoohooh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1fnXOQAUm7qV5LFj4j2uCEBmBcdGVhkpY")
            },
            new Song
            {
                Name = "Patate de forain évidemment",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Patate_de_forain_evidemment.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aeo_cQwrO8snM5fqFVy4cVbiuPcDld4M")
            },
            new Song
            {
                Name = "Yes on est tous copains",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Yes_on_est_tous_copains.mp3",
                Url = new Uri("https://drive.google.com/file/d/12J5yswBTmc0WHQju5KsVwwCpsjkbbZxw/view?usp=sharing")
            },
            new Song
            {
                Name = "Ablablablablablabli",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Ablablablablablabli.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jXnj_kaYL_ho7sK1H2ML2v3BG0vUY-H6")
            },
            new Song
            {
                Name = "Chanson Trovato le nabot",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Chanson_Trovato_le_nabot.mp3",
                Url = new Uri("https://drive.google.com/open?id=1_K4fRN8MCZl4dFLMy-OANmrashIv8Rk6")
            },
            new Song
            {
                Name = "Scorvol jouissance",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Scorvol_jouissance.mp3",
                Url = new Uri("https://drive.google.com/file/d/1rnGe0aicyk5McABddyspyeIfq5BwwX1c/view?usp=sharing")
            },


            new Song
            {
                Name = "Oulala oulala",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Oulala_oulala.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MryqUjPFQIxxqXyj-EoaxlgzgDueSzCA")
            },
            new Song
            {
                Name = "Bitentronc esquive les attaques",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Bitentronc_esquive_les_attaques.mp3",
                Url = new Uri("https://drive.google.com/file/d/1e0T5am8zZaIDj2R_6fzCPxh2skaRkoYX/view?usp=sharing")
            },
            new Song
            {
                Name = "Bitentronc jugement",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Bitentronc_jugement.mp3",
                Url = new Uri("https://drive.google.com/file/d/1J9xQ4zT5wy7vu-s1YcYoKUNBH_iM9Ohl/view?usp=sharing")
            },
            new Song
            {
                Name = "Combat qui dure 30 minutes",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Combat_qui_dure_30_minutes.mp3",
                Url = new Uri("https://drive.google.com/file/d/1LJrS4Y595zDaxUAIAoHKLvniEnK-ivzC/view?usp=sharing")
            },
            new Song
            {
                Name = "Est ce que j'ai des black",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Est_ce_que_jai_des_black.mp3",
                Url = new Uri("https://drive.google.com/file/d/1fokdqEJ7B87AcZfkz0okC9_pAbl22Tt0/view?usp=sharing")
            },
            new Song
            {
                Name = "Euheuheuheu ohohoho",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Euheuheuheuohohoho.mp3",
                Url = new Uri("https://drive.google.com/file/d/1xMvzrYYZc1kkXavKeH54vGMKFmp_cvR3/view?usp=sharing")
            },
            new Song
            {
                Name = "Hihihihihihi",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Hihihihihihi.mp3",
                Url = new Uri("https://drive.google.com/file/d/1CSYn3RPOflCRZhZOh9hqSEfbDg1Jesgd/view?usp=sharing")
            },
            new Song
            {
                Name = "Je l'ai capturé à la filet ball",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Jlai_capture_a_la_filet_ball.mp3",
                Url = new Uri("https://drive.google.com/file/d/1brjWhZK4esebphxTm4NkSd8tqXOQ9-_a/view?usp=sharing")
            },
            new Song
            {
                Name = "La mort de pipomiel",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/La_mort_de_pipomiel.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ychDSjdVBRMy_U_qUZPdCvCGncbzQJ5G/view?usp=sharing")
            },
            new Song
            {
                Name = "Maidujdizksjdie",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Maidujdizksjdie.mp3",
                Url = new Uri("https://drive.google.com/file/d/1cGrzrZvYbDzfIY2kbK2h92pn0IlJRfKJ/view?usp=sharing")
            },
            new Song
            {
                Name = "Mais abruti de ...",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Mais_abruti_de.mp3",
                Url = new Uri("https://drive.google.com/file/d/140ldxlrYxaxLv12zVgCl4csmey6DYwWJ/view?usp=sharing")
            },
            new Song
            {
                Name = "Pied voltige Terrakium",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Pied_voltige_terrakium.mp3",
                Url = new Uri("https://drive.google.com/file/d/1065T06nN4iyqysGaTb1IR83UJBBNIK_c/view?usp=sharing")
            },
            new Song
            {
                Name = "Simularbre partage force",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/Simularbre_partage_force.mp3",
                Url = new Uri("https://drive.google.com/file/d/1PrQrDvLrjx87OIghpXMo8m_GqNVpGWDN/view?usp=sharing")
            },

            new Song
            {
                Name = "Un feu d'artifice avec ta grosse mère",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/un_feu_dartifice_avec_ta_grosse_mere.mp3",
                Url = new Uri("https://drive.google.com/file/d/1DvBjXX3_Nx9W5IiN-0B8PV4FJcHWPqXr/view?usp=sharing")
            },
            new Song
            {
                Name = "Rhinastoc",
                Image = ImagePokemonX,
                Category = CategoryLivePokemonX,
                Mp3 = "X/rhinastoc.mp3",
                Url = new Uri("https://drive.google.com/file/d/1VVWvyQihJXp-lOfuFCwxEi0Ru4gyedmg/view?usp=sharing")
            },
            
            #endregion
            #region Live Pokemon Epee FAIT
            new Song
            {
                Name = "Atchoumdrong",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/atchoumdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1nvTDOSg5aePTcusyXBxva5pSosTXw_ul")
            },
            new Song
            {
                Name = "Badge d'ancienneté de 10 ans",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/badge_d_anciennete_de_10_ans.mp3",
                Url = new Uri("https://drive.google.com/open?id=1OXVKB7eZ3R3gMbFOwZkoCWpbKFFk9g4J")
            },
            new Song
            {
                Name = "Chansondrong",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/chanson_drong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1loROzP2iHct4q79OETDPLpK-kKazsHBZ")
            },
            new Song
            {
                Name = "Hyper malpoli",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/hyper_malpoli.mp3",
                Url = new Uri("https://drive.google.com/open?id=1b05XqfgHGTp4WO2xXFt0lmTP3o_Tp1I3")
            },
            new Song
            {
                Name = "Il fait super mal",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/il_fait_super_mal.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aNPmmcKOUX7NACncPZqNpUYRK401jvqc")
            },
            new Song
            {
                Name = "Je vous ai fisté l'anus",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/je_vous_ai_fiste_l_anus.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Kh_zLE7zT3al2IDYuN-P8-cnBC4UxPHI")
            },
            new Song
            {
                Name = "Kikounette vs Concombaffe",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/kikounette_vs_concombaffe.mp3",
                Url = new Uri("https://drive.google.com/open?id=1u654Wx7DED1obEdrE8H3nqMJf4Y0YoFt")
            },
            new Song
            {
                Name = "Ma maman est trop sexy olalahhh",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/ma_maman_est_trop_sexy_olalahhh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Okv79PzhIQFx3ovnyLYE24KY0VBTe2ie")
            },
            new Song
            {
                Name = "Métronome empal'korne",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/metronome_empal_korne.mp3",
                Url = new Uri("https://drive.google.com/open?id=1QXa-qkcjgwV94U3833L4C1V8P-w5gebJ")
            },
            new Song
            {
                Name = "Moi c'est persil",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/moi_c_est_persil.mp3",
                Url = new Uri("https://drive.google.com/open?id=1s-rV1Vr4MQfdNTDuizDQgzJKk5HS4ZP0")
            },
            new Song
            {
                Name = "Moumoutondrong",
                Image = ImagePokemonEpee,
                Category = CategoryImitation,
                Mp3 = "Epee/moumoutondrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1oX-SOK4aK_9Odtge6dBVP8uFIavzF6xJ")
            },
            new Song
            {
                Name = "On a le Pokemon de zone",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/on_a_le_pokemon_de_zone.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rZO2ppTJHt5IPyVb_gV3l-Yh5CaNmMfs")
            },
            new Song
            {
                Name = "On va se faire un look stylé",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/on_va_se_faire_un_look_style.mp3",
                Url = new Uri("https://drive.google.com/open?id=1v6p3P79FFQliikT11BiWGi0FgNox8z7-")
            },
            new Song
            {
                Name = "Pikachudrong",
                Image = ImagePokemonEpee,
                Category = CategoryImitation,
                Mp3 = "Epee/pikachudrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1nlZ4KqpWu45tRLA8m8gipU8GUnVuAfw4")
            },
            new Song
            {
                Name = "Pourquoi je me fais enculer par mon nuzlocke",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/pourquoi_je_me_fais_enculer_par_mon_nuzlocke.mp3",
                Url = new Uri("https://drive.google.com/open?id=1B3fP55TdTd7yptk_6YV8_F6_F9xppzOy")
            },
            new Song
            {
                Name = "Sucemomax",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/sucemomax.mp3",
                Url = new Uri("https://drive.google.com/open?id=1w09-QvJoFjhJzJLuoRu6RR7j248CxM0n")
            },
            new Song
            {
                Name = "Sucreinedrong",
                Image = ImagePokemonEpee,
                Category = CategoryImitation,
                Mp3 = "Epee/sucreinedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1kLUJH_WmptNOQyQvNMcRpENlFMfw52OE")
            },
            new Song
            {
                Name = "Tu veux pas me branler",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/tu_veux_pas_me_branler.mp3",
                Url = new Uri("https://drive.google.com/open?id=1quloC74h_aKLXpJMZq5V1LuhSqSUPYj2")
            },
            new Song
            {
                Name = "Un nouveau Pokemon",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/un_nouveau_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1t_-042ayscasF-MG66zM_LJHmOtQQTS7")
            },
            new Song
            {
                Name = "Journée rempli branlette accomplie",
                Image = ImagePokemonEpee,
                Category = CategoryLivePokemonEpee,
                Mp3 = "Epee/journee_rempli_branlette_accomplie.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mY_JFhRuPHW0rxLDkGpxY9FnlPDkoui9")
            },
            #endregion

            #region Live Pokemon Platine Full Random FAIT
            new Song
            {
                Name = "ANUSETTE",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/ANUSETTE.mp3",
                Url = new Uri("https://drive.google.com/open?id=1GfempjMUiAaBdqlqFLHcSVoI-2bbklXI")
            },
            new Song
            {
                Name = "Apparition de Celebi",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/apparition_de_celebi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1eXDNncVaquNmPIC4k-aVwp-VOYdmTT4M")
            },
            new Song
            {
                Name = "Apprendre queue de fer",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/apprendre_queue_de_fer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mFGPJU6ynRCKCm3Ow6nohnUyA9jshUoY")
            },
            new Song
            {
                Name = "Bruit d'une attaque",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryImitation,
                Mp3 = "PlatineFullRandom/bruit_dune_attaque.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uL2d-rqPYh262XllcRRr2hmpBkMdIyWC")
            },
            new Song
            {
                Name = "Chehdrong",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/chehdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1X3Bsr0edADznLkpkQ0bbWNhehR7-bLUo")
            },
            new Song
            {
                Name = "Clavier sauvage",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/clavier.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YB9cWhsapyRl65t6yx__QhbEGWEqu6h0")
            },

            new Song
            {
                Name = "Ça existe pas votre ville de merde",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/ca_existe_pas_votre_ville_de_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VdfExDHh8j3eeU2CV5Qp-n5V0r6HmbHQ")
            },
            new Song
            {
                Name = "Coup critique ernestine",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/coup_critique_ernestine.mp3",
                Url = new Uri("https://drive.google.com/open?id=1q09XM24o6IxbHfFDNdf_fU9-492XppuL")
            },
            new Song
            {
                Name = "Cul de jus",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/cul_de_jus.mp3",
                Url = new Uri("https://drive.google.com/open?id=16cx4dyBJWgqB179Yo90fXqsZgBRbN4bn")
            },
            new Song
            {
                Name = "Elephandrong",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryImitation,
                Mp3 = "PlatineFullRandom/elephandrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1XIP7TjYX10smfZKDEDIuFv3oBazG9p2K")
            },
            new Song
            {
                Name = "Enculé par méganium",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/encule_par_meganium.mp3",
                Url = new Uri("https://drive.google.com/open?id=10r1rN_Y1mE92JYEHnq0W1nOgjBOGvmeP")
            },
            new Song
            {
                Name = "Explosif",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/explosif.mp3",
                Url = new Uri("https://drive.google.com/open?id=1JRUcrLiUVQu0hFF40U39Dfs5tXUI6MHv")
            },
            new Song
            {
                Name = "Fifou le wesh",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/fifou_le_wesh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tmGlGBZkIUm28q2pR1iCX2EIHgEK93vT")
            },
            new Song
            {
                Name = "Hipopotasdrong",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryImitation,
                Mp3 = "PlatineFullRandom/hipopotasdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=16V9XLv8L8xAsMQ2MnopbFmIRheQmBk5s")
            },
            new Song
            {
                Name = "Melokrikdrong",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryImitation,
                Mp3 = "PlatineFullRandom/immitation_melokrik.mp3",
                Url = new Uri("https://drive.google.com/open?id=18QBHXwkuIir54ehPcLXcm4MZrGBV2t6U")
            },
            new Song
            {
                Name = "Je pète ça pue",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/je_pete_ca_pue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1DNqZsgAitLSMVGwIpS7ReH7wWKvXPA1J")
            },
            new Song
            {
                Name = "Je vais me mettre à poil",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/je_vais_me_mettre_a_poil.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ClaHSN935EpqIWSZX-feiZ3HkyK9d416")
            },
            new Song
            {
                Name = "Jsuis pas concentré",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/jsuis_pas_concentre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hUWeOaUoyWiNPqLZR1iJBD0X0XAxs44V")
            },
            new Song
            {
                Name = "Les gilets jaunes ne sont pas méchants",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/les_gilets_jaunes_ne_sont_pas_mechants.mp3",
                Url = new Uri("https://drive.google.com/open?id=1fLm7tAbv-09VCk--2dalOXeDoAvMjqEZ")
            },
            new Song
            {
                Name = "Main dans le slip",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/main_dans_le_slip.mp3",
                Url = new Uri("https://drive.google.com/open?id=1R-jtampOa2A6q-1gTF5A5LP_52SZVPq1")
            },
            new Song
            {
                Name = "Merde y a le pénis",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/merde_ya_le_penis.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SN-RdEVU6jrWsrDSxuxHMsq4KPm8dFLL")
            },
            new Song
            {
                Name = "Nameouie",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/nameouie.mp3",
                Url = new Uri("https://drive.google.com/open?id=1OXK7hzYbNP2TpPs4fPNrH_5RvPl3ONDI")
            },
            new Song
            {
                Name = "Oh ta mère",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/Oh_ta_mere.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lc4r_kBMAG7vNiAoqGBfDo3qvWAAH6Go")
            },
            new Song
            {
                Name = "Pas concentré",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/pas_concentre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1BOKMDJ-cK3Ew2dXwnIEoHpKQ7XeTvyNp")
            },
            new Song
            {
                Name = "Pichou",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/pichou.mp3",
                Url = new Uri("https://drive.google.com/open?id=1FStiea510nPpfxCcO-Z5F1IS1YvxSV4q")
            },
            new Song
            {
                Name = "Plume dans le cul",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/plume_dans_le_cul.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vpoQ9jxMggXr9O9nF5-Id1CQVL-aaDHM")
            },
            new Song
            {
                Name = "Poupi",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/Poupi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1HSgKOQ728Zxo04UbMuZqWlDCkM91DYJ0")
            },
            new Song
            {
                Name = "PR",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/PR.mp3",
                Url = new Uri("https://drive.google.com/open?id=1oKb9aaWpfcK9Rj9OK-GCgU2l46wsV1r6")
            },
            new Song
            {
                Name = "Puis je suis MARS",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/puis_je_suis_MARS.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ks7tMkBBLa7m4Pnhc3600IPDOTDt7ypf")
            },
            new Song
            {
                Name = "Rire foireux",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/rire_foireux.mp3",
                Url = new Uri("https://drive.google.com/open?id=14OZ-CRrXiQ2N9y6WCONAy_oytwhixOEN")
            },
            new Song
            {
                Name = "Singedrong",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryImitation,
                Mp3 = "PlatineFullRandom/singedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1UsCS1S44SEGbNc5xjt-_3LLyZVUBx7mW")
            },
            new Song
            {
                Name = "Son cris porfond",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/son_cris_porfond.mp3",
                Url = new Uri("https://drive.google.com/open?id=14aWrzKXpiXCGRnopm3zWcOeHo_zftbPD")
            },
            new Song
            {
                Name = "Ta queue",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/ta_queue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ad3VpcSxDxnG9PoCrSCIJXw43tK28TDv")
            },
            new Song
            {
                Name = "Ta queue qui a apprit anti brume",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/ta_queue_qui_a_apprit_anti_brume.mp3",
                Url = new Uri("https://drive.google.com/open?id=15wauM1tGFs-r2sZyUucalS8VKchbpLWr")
            },
            new Song
            {
                Name = "Terry",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/terry.mp3",
                Url = new Uri("https://drive.google.com/open?id=1DYK8bW_E8jMJ23LzzOfwyi87s_9UJsHE")
            },
            new Song
            {
                Name = "Trop d'amour",
                Image = ImagePokemonPlatineFullRandom,
                Category = CategoryLivePokemonPlatineFullRandom,
                Mp3 = "PlatineFullRandom/trop_damour.mp3",
                Url = new Uri("https://drive.google.com/open?id=12Jgi3ozhgydjdr6wQvCV-WIz2moAu_RC")
            },
            #endregion
            #region Live Pokemon Clover FAIT
            new Song
            {
                Name = "Accident Cicatrice",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/Accident_cicatrice.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lgel4tgAbeR6wfvhrRi25za8VKXYC7MZ")
            },
            new Song
            {
                Name = "Oh une feuille",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/Oh_une_feuille.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mmLAhCw-ZbElEIYQbvkN67Y7yy2rndeo")
            },
            new Song
            {
                Name = "Ouais je pense que la bite tue",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/Ouais_je_pense_que_la_bite_tue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ma-AHBImH6xwG3bz89IxK6UnZBbsz_XL")
            },
            new Song
            {
                Name = "Cris",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/cris.mp3",
                Url = new Uri("https://drive.google.com/open?id=12RQXlY9fpsRCg1XVfH8KOeYu4fRux56p")
            },
            new Song
            {
                Name = "Oh il est adorable, Mais il est mort",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/oh_il_est_adorable__Mais_il_est_mort.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lnhr1S_yfjm9PJE5oMY6qdXmOywOQWek")
            },
            new Song
            {
                Name = "On voit pas les noms",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/on_voit_pas_les_noms.mp3",
                Url = new Uri("https://drive.google.com/open?id=1-a0svJ7PJ2_CwJ0sXoFADCddvMvR1D93")
            },
            new Song
            {
                Name = "COUP CRITIQUE!",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/COUP_CRITIQUE.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hRNu0YjF65UgvLk13SIoyam8_yoqsVGx")
            },
            new Song
            {
                Name = "Dududu",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/dududu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MWdvd8CCVGBHW_gPIHod8tVmBEFjiav1")
            },
            new Song
            {
                Name = "Poke center pokketto monstaa",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/poke_center_pokketto_monstaa.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YAtPaQe8zwfScdnBzI23wvfVpsRAV6HT")
            },
            new Song
            {
                Name = "Alors GOOGLE",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/alors_GOOGLE.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VpOrmHtByCum1s1o2WeNTudggBGQfxHe")
            },
            new Song
            {
                Name = "Bruit étrange",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/bruit_etrange.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tRr4NEHQbkLpJj8Va4ESCOgknuat5vD_")
            },
            new Song
            {
                Name = "Remise en question",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/remise_en_question.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qrnyzx-tvN_zT95aKloIfIs7DegJXfbt")
            },
            new Song
            {
                Name = "Rugissement",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/rugissement.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hn-FTdY3Cr-YSWFDdruBxrQy_OcOgJCN")
            },
            new Song
            {
                Name = "Tuto avoir le jeu",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/tuto_avoir_le_jeu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1n7gRqkcGnfKGpCtu-wl5DYBZWxMUUhO_")
            },
            new Song
            {
                Name = "Allez noraj",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/Allez_noraj.mp3",
                Url = new Uri("https://drive.google.com/open?id=1V1CN7OeGAE1sO6-_qwCjVXrGwQcJtBjb")
            },
            new Song
            {
                Name = "Chanson du pain",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/chanson_du_pain.mp3",
                Url = new Uri("https://drive.google.com/open?id=1dhjdvKF_bAJMVkFc87iYB0UBeL5wF8r7")
            },
            new Song
            {
                Name = "Noraj de l'orage",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/noraj_de_lorage.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SDH0i9j4fgoaZKB-UVEhk1ZWoRZqaocm")
            },
            new Song
            {
                Name = "Lelicobite",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/lelicobite.mp3",
                Url = new Uri("https://drive.google.com/open?id=15dYT4b5RDz7Lsq_CnqWbMAQGMVfA7WGl")
            },
            new Song
            {
                Name = "Les égouts de merde",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/les_egouts_de_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qWLbvjFmsGUhpGZzFhDk8TYwZqJ_-E9E")
            },
            new Song
            {
                Name = "Poisson fesse",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/poisson_fesse.mp3",
                Url = new Uri("https://drive.google.com/open?id=1f6M9K3icAf2KZnSpiI4PmXvQFF9gyXN1")
            },
            new Song
            {
                Name = "Aaaaaaarrgh",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/Aaaaaaarrgh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1cNnykzmC_TDJvEPcWA6W4VE8hZy17A7i")
            },
            new Song
            {
                Name = "God michet",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/god_michet.mp3",
                Url = new Uri("https://drive.google.com/open?id=1GQwE8DIsvHnS46EWOAcrhiJQ6YH9VWbI")
            },
            new Song
            {
                Name = "Les labirynthes c'est de la merde",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/les_labirynthes_cest_de_la_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1P8vIGrD4sKsk5V0aydgM-ypdurAW8XAO")
            },
            new Song
            {
                Name = "Ça pleut sous la pluie",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/ca_pleut_sous_la_pluie.mp3",
                Url = new Uri("https://drive.google.com/open?id=17CN14-h8TSRzWVklIby2Bq307m8Qe5u2")
            },
            new Song
            {
                Name = "Retournement de situation",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/retournement_de_situation.mp3",
                Url = new Uri("https://drive.google.com/open?id=1AHG7ew0cBV3tOZytwSI-pH3zz-yUCChg")
            },
            new Song
            {
                Name = "Ta mère de merde",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/ta_mere_de_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1cFP5b3bOfnETHu1wvMCPSQW6rNJFl_CX")
            },
            new Song
            {
                Name = "The grudge",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/the_grudge.mp3",
                Url = new Uri("https://drive.google.com/open?id=1I1GK4ty2COtPe6q8KdtfBC7d_XtBEDZU")
            },
            new Song
            {
                Name = "AH enroué",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/AH_enroue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1HatDSpL-2BhyKAo5wnWQFplSnumb5qS7")
            },
            new Song
            {
                Name = "Hallelouya",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/hallelouya.mp3",
                Url = new Uri("https://drive.google.com/open?id=1p_m0lEaYIC3unT_JCbidhFMHvsyiu_uc")
            },
            new Song
            {
                Name = "Kamehameha",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/kamehameha.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vHeIJHaNrdU2uu0DVrcRemeFWYsrneMu")
            },
            new Song
            {
                Name = "Kawaii desu",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/kawaii_desu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1O6DggVSktHxBHLerQDYYP48dt6NSKPdx")
            },
            new Song
            {
                Name = "Si j'avais sus",
                Image = ImagePokemonClover,
                Category = CategoryLivePokemonClover,
                Mp3 = "Clover/si_javais_sus.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MyB7vf_06_BB8ySZzIapSDTR4BlELINU")
            },
            new Song
            {
                Name = "Darude",
                Image = ImagePokemonClover,
                Category = CategoryMusique,
                Mp3 = "Clover/Darude.mp3",
                Url = new Uri("https://drive.google.com/open?id=17APQSr4ph4rAPsVGeLkai8jUBfXyVCN7")
            },
            
            #endregion
            #region Live Pokemon Blaze Black
            new Song
            {
                Name = "Lianajadrong",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryImitation,
                Mp3 = "BlazeBlack/Lianajadrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1REQoSr84UeqG0SiUVXOy_RoC-MVg82ug")
            },
            new Song
            {
                Name = "Mais lachez moi la",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/Mais_lachez_moi_la.mp3",
                Url = new Uri("https://drive.google.com/open?id=1U8sN8Qo7xPR_N41ampx4y4rlA7szIsLm")
            },
            new Song
            {
                Name = "Oh bitenbois",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/Oh_bitenbois.mp3",
                Url = new Uri("https://drive.google.com/open?id=1q818LSyZf8lthI3bHX0_FG2ca_E_38Zs")
            },
            new Song
            {
                Name = "Bianca",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/Bianca_nique_ta_race.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uOumKmXQK65WrFySWMVZOc52xLiIJG0S")
            },
            new Song
            {
                Name = "Branettedrong",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryImitation,
                Mp3 = "BlazeBlack/Branettedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tDEP-I4wo4qCL6Q644mPOzIAIB88SRe6")
            },
            new Song
            {
                Name = "Fildrong parle à le chat",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/Fildrong_parle_a_le_chat.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lGJgThsNmtKCxc2KU6nyBm4f1PcPQyTy")
            },
            new Song
            {
                Name = "Imaginez",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/imaginez.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IrUkndvr3Tn58G6Ro2sW0DvpIbBr5Bkm")
            },
            new Song
            {
                Name = "Japonaisdrong",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryImitation,
                Mp3 = "BlazeBlack/Japonaisdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1TRGScuLUpFcrwGLKllqNIeoUqyqz8THe")
            },
            new Song
            {
                Name = "La ceinture force",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/la_ceinture_force.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MWodgaRq8jChz8eKaIlX6_eRdASbNM8Q")
            },
            new Song
            {
                Name = "La merde qu'il est entrain de chier",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/la_merde_quil_est_entrain_de_chier.mp3",
                Url = new Uri("https://drive.google.com/open?id=1_m8mYVZv8AX6Al8ETIAapyRJMdUXQD9b")
            },
            new Song
            {
                Name = "Leopardrong",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryImitation,
                Mp3 = "BlazeBlack/leopardrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1cs0vxYqIbPbFLsVI0dXXuej676oAM2mF")
            },
            new Song
            {
                Name = "Merci a tanké",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/merci_a_tanke.mp3",
                Url = new Uri("https://drive.google.com/open?id=1XE6B0mqmlZg2bR-SfrlC0EYArGeKKESN")
            },
            new Song
            {
                Name = "Perdudrong",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/Perdudrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wzLYLqJuI3nC6YbXu2hfFmGBklqVaIV_")
            },
            new Song
            {
                Name = "Plein d'émotivation",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/plein_demotivation.mp3",
                Url = new Uri("https://drive.google.com/open?id=1B00jMXrYHoSvWll-yzUi_YQHP8Jq4Vqp")
            },
            new Song
            {
                Name = "Trompé d'attaque",
                Image = ImagePokemonBlazeBlack,
                Category = CategoryLivePokemonBlazeBlack,
                Mp3 = "BlazeBlack/TROMPE_DATTAQUE.mp3",
                Url = new Uri("https://drive.google.com/open?id=12SIj_-q8aqHGidkv14iP25LtKyjoVo8p")
            },

            #endregion
            #region Live Pokemon Eclat pourpre FAIT
            new Song
            {
                Name = "C'est en français",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/C_est_en_francais.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xbxhvJbXqhAY5MiTge7hrNf-VBNOo6vU")
            },
            new Song
            {
                Name = "Ça se fait pas",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/ca_se_fait_pas.mp3",
                Url = new Uri("https://drive.google.com/open?id=16k7-zNzbwVeh2yGDrhoXfS-A3s8R8dVk")
            },
            new Song
            {
                Name = "La souplesse",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/la_souplesse.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KuhuvKC4JTcDY-jGWOsqUY0gSPR8tEtx")
            },
            new Song
            {
                Name = "Lol fun",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/lol_fun.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Ickvmk0hEp8DXjfbOj1_agSNKW_vxLO7")
            },
            new Song
            {
                Name = "Ta mère",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Ta_mere.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MJ57uyIzqcJe4CyvIzpzwtSWm7M4wr_F")
            },
            new Song
            {
                Name = "Un shiny",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/un_shiny.mp3",
                Url = new Uri("https://drive.google.com/open?id=1LbXxhx8Zamqy6hsTNuFrHol1lkxEWB_c")
            },
            new Song
            {
                Name = "AH",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/AH.mp3",
                Url = new Uri("https://drive.google.com/open?id=1iM1mvfPTyl3pq35cj9L1fO3P6W1v7Atx")
            },
            new Song
            {
                Name = "Ah Vazlynn",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Ah_Vazlynn.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tM4uV4aphD9BVO_vWJ6Hv1AbN09TkeKl")
            },
            new Song
            {
                Name = "Arène impossible",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/arene_impossible.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lw9sfI1Q4olmx8MSLLoFZYhDqwiBdiTJ")
            },
            new Song
            {
                Name = "Double pied anus",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Double_pied_anus.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vK4pgMAW6xKEjc7ytqWt_sY9VoLriPM5")
            },
            new Song
            {
                Name = "Fautes d'authographe",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/fautes_authographe.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Dm_BuL4xJG0iEcsDAYJi5jledrfd1GLg")
            },
            new Song
            {
                Name = "Je t'ai niquer mec",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/je_t_ai_niquer_mec.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Sq9KVxBMiCbJLWa4a7WnbcjjYmilfMx1")
            },
            new Song
            {
                Name = "Jouissance",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Jouissance.mp3",
                Url = new Uri("https://drive.google.com/open?id=1kH8f-nQGA_jE5jLCb0SpDl--ExgB3ccG")
            },
            new Song
            {
                Name = "Noraj",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Noraj.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wfhC0IHwE63PVKpKho-U7OjKjC7NN4Bl")
            },
            new Song
            {
                Name = "One shot",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/one_shot.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ZiJiLq5pp_LNWRpKvvSn7zAd2BS8HIjo")
            },
            new Song
            {
                Name = "Pris la tête",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/pris_la_tete.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Vk-malP4CkR7ht5XGjV-VsHK0VO9yRoC")
            },
            new Song
            {
                Name = "Titre pute à clic",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Titre_pute_a_clic.mp3",
                Url = new Uri("https://drive.google.com/open?id=1pz7QbDkzYJh25wJ2hDHuU0fzKpNrqZ-L")
            },
            new Song
            {
                Name = "WOUAHOU",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/WOUAHOU.mp3",
                Url = new Uri("https://drive.google.com/open?id=1he5Cjw3DCKGPvcOBh32MBO1HnAiGofot")
            },
            new Song
            {
                Name = "Au bah casse toi",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/au_bah_casse_toi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1J2PJkkpjs2up6-WsrJKol6OAD4u1DTWZ")
            },
            new Song
            {
                Name = "Hors de question",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/hors_de_question.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Df5uxnD8gQ9nueurKeLgvLx2GaUGhdfT")
            },
            new Song
            {
                Name = "L'arène la plus facile",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/l_arene_la_plus_facile.mp3",
                Url = new Uri("https://drive.google.com/open?id=1UCs7FV_83PE5UCI0q_EuCWReGIwv2hK6")
            },
            new Song
            {
                Name = "Métronome explosion",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/metronome_explosion.mp3",
                Url = new Uri("https://drive.google.com/open?id=1phzXbjRrgqGpDPZwlL3yiQAMT172_ZTO")
            },
            new Song
            {
                Name = "Non putain",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/non_putain.mp3",
                Url = new Uri("https://drive.google.com/open?id=1S9FtDqYgmqvIdLah6Qd9kdiFw9OT7F0w")
            },
            new Song
            {
                Name = "Pourquoi tu es là ?",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/pourquoi_tu_es_la.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Deqs70QW3caUAgHHKUgKfPmsd7zxnzfy")
            },
            new Song
            {
                Name = "Ça sent le combat",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/ca_sent_le_combat.mp3",
                Url = new Uri("https://drive.google.com/open?id=1EtrvV4CVqExJDOVnNW6A-Cny63Q6mqj2")
            },
            new Song
            {
                Name = "Tous une bite",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/tous_une_bite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MSUgiY1FTyMa6o63jFY_ctVjMW-EluQk")
            },
            new Song
            {
                Name = "Argh",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/argh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1PUstUL4lY0U_aiF6v-jj1CnMvRsPHUmV")
            },
            new Song
            {
                Name = "Bla bla bla bla",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/bla_bla_bla_bla.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SfkKqDkuYN2HGys5XeFwSD0QxHRu9zGd")
            },
            new Song
            {
                Name = "Respecte les anciens",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/respecte_les_anciens.mp3",
                Url = new Uri("https://drive.google.com/open?id=134YeFrZvjzhXQ5CrB_oHHyq7hl2EtXdW")
            },
            new Song
            {
                Name = "Arretez de faire des fautes",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/arretez_de_faire_des_fautes.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wa_kSXeJlZsoGyeBenbJ-WY0mPdMkpHy")
            },
            new Song
            {
                Name = "Mais meurt connard",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/mais_meurt_connard.mp3",
                Url = new Uri("https://drive.google.com/open?id=1fxiWz5d0GQVrZu8hTl1eZATP2OHtBiOc")
            },
            new Song
            {
                Name = "Vous devriez acheter des potions",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/vous_devriez_acheter_des_potions.mp3",
                Url = new Uri("https://drive.google.com/open?id=1l5moViQ620rH6fq3nx8hPh5XvUMJXmCJ")
            },
            new Song
            {
                Name = "Merde",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/Merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1QEf7VjdgZEnujHiEyYLG1bA09mXQA0P3")
            },
            new Song
            {
                Name = "Reflet x3",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/reflet_x3.mp3",
                Url = new Uri("https://drive.google.com/open?id=1R34jjDTN1Qb3TtU3WWqTa12gsQTE1AWt")
            },
            new Song
            {
                Name = "Parce que je suis stratège",
                Image = ImagePokemonEclatPourpre,
                Category = CategoryLivePokemonEclatPourpre,
                Mp3 = "EclatPourpre/parce_que_je_suis_stratege.mp3",
                Url = new Uri("https://drive.google.com/open?id=15TycL9pUfAV5JkW6AFIVO8jQhTh7VFaB")
            },


            #endregion
            #region Live Pokemon Mind Crystal FAIT
            new Song
            {
                Name = "Alors les dégats",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/alors_les_degats.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SUA0dOO4pvSibcBkOsFKPWc0OQ_8qwN2")
            },
            new Song
            {
                Name = "Bonjour",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/bonjour.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YxlaXTvC1Dv4_l4TORbQkzOVTqh1vFPa")
            },
            new Song
            {
                Name = "Laisse ma mère",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/laisse_ma_mere.mp3",
                Url = new Uri("https://drive.google.com/open?id=1WPmq_2EzRAp_oOxvmV-enhhAMTyTFRrZ")
            },
            new Song
            {
                Name = "Fildrong tue ponyta",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/Fildrong_tue_ponyta.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vlrVHloUswrdohRDJJcZAiYiwKCV38bX")
            },
            new Song
            {
                Name = "Gamin farouk",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/gamin_farouk.mp3",
                Url = new Uri("https://drive.google.com/open?id=169RfdkA1WjIXW_igv6WT2BpYTmRlnDcM")
            },
            new Song
            {
                Name = "Les sprites",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/les_sprites.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vFWmVSCN8XUJ7w9qNeq5QLhSL1XmSDHI")
            },
            new Song
            {
                Name = "Nique ta mère jeu de merde",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/nique_ta_mere_jeu_de_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uzXk9BmpEWCDJJesQVMl1qJ8Q_BieJnZ")
            },
            new Song
            {
                Name = "WTF les amis",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/wtf_les_amis.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ILA3emv6TgKZhd2NChmQrRI2cQ9DSPtZ")
            },
            new Song
            {
                Name = "C'est pas juste",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/c_est_pas_juste.mp3",
                Url = new Uri("https://drive.google.com/open?id=1oegMobseo9kl-FE1LSm6uAIWKB1_gLDt")
            },
            new Song
            {
                Name = "Donner son numéro",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/donner_son_numero.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tGoy61NBT9Pgfo6W9aX6ySB36Sc_5Wxp")
            },
            new Song
            {
                Name = "Il a tank",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/il_a_tank.mp3",
                Url = new Uri("https://drive.google.com/open?id=1BocXvwEqd1Ei_lw_KXFNtxT3EKoj1vQm")
            },
            new Song
            {
                Name = "Pire ennemi",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/pire_ennemi.mp3",
                Url = new Uri("https://drive.google.com/open?id=13pH_nxFmrCjHCIltPusmS-RAvHOtPwjo")
            },
            new Song
            {
                Name = "WHAT THE FUUUCK",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/WHAT_THE_FUUUCK.mp3",
                Url = new Uri("https://drive.google.com/open?id=1m_nsgMVeMUeNq9nGDgTiox3HIY0L4YG_")
            },
            new Song
            {
                Name = "Consomme pas de la drogue",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/consomme_pas_de_la_drogue.mp3",
                Url = new Uri("https://drive.google.com/open?id=18IBDbXAX0oEQzTWj1FQ_dZZI_5VJF0nx")
            },
            new Song
            {
                Name = "Dimitri du dimanche",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/dimitri_du_dimanche.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SHKRztaJnbZjQDPLbpf3vqvxGgWNpl35")
            },
            new Song
            {
                Name = "Flemme de changer les sprites",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/flemme_de_changer_les_sprites.mp3",
                Url = new Uri("https://drive.google.com/open?id=1g1NM4-jIySm5oF9zKjPA34JbOkY8ey5h")
            },
            new Song
            {
                Name = "Il veut faire cognobidon",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/il_veut_faire_cognobidon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1yzwZiR3lci_DWCNESPndJ3prv6wsV1Kd")
            },
            new Song
            {
                Name = "Je suis prêt pour me faire défoncer",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/je_suis_pret_pour_me_faire_defoncer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1a_Vtuim9qPxoQgcWjgsLnmqBozOIMDg3")
            },
            new Song
            {
                Name = "La taille de cette fontaine",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/la_taille_de_cette_fontaine.mp3",
                Url = new Uri("https://drive.google.com/open?id=1_fWxT8FjAGjDQ6-SDmWIjt4Q1BcJhtwB")
            },
            new Song
            {
                Name = "Moi aussi je pleurerais",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/moi_aussi_je_pleurerais.mp3",
                Url = new Uri("https://drive.google.com/open?id=15big7FrlrACMPuKH80WHdnrxg4FstAK0")
            },
            new Song
            {
                Name = "Mongoldrong est de retrour",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/mongoldrong_est_de_retrour.mp3",
                Url = new Uri("https://drive.google.com/open?id=1zAszviSL5Aisbxtj2FaGTAWGfxzpm9ac")
            },
            new Song
            {
                Name = "Oh le nuzlocke",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/oh_le_nuzlocke.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xuPLWUhKLpRr7bRUtiFy-U3nZasDZheC")
            },
            new Song
            {
                Name = "Arretez avec ces sprites de merdes",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/arreter_avec_ces_sprites_de_merdes.mp3",
                Url = new Uri("https://drive.google.com/open?id=172CFsgRApWyWxVyNvqBX_V_K-Y2bwTp8")
            },
            new Song
            {
                Name = "Dans mortimer il y a mer",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/dans_mortimer_il_y_a_mer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1AbboyBd72nhk6wC2n-FGw9yxAgyR4A1n")
            },
            new Song
            {
                Name = "Dépit dépité",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/depit_depite.mp3",
                Url = new Uri("https://drive.google.com/open?id=10fKRwfHj1njeSpSOaLqiRsi6ZJXko59d")
            },
            new Song
            {
                Name = "Je vais te",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/je_vais_te.mp3",
                Url = new Uri("https://drive.google.com/open?id=1exWZCZ3fa5zw2kAN7cE_tdTMYAfUlTjv")
            },
            new Song
            {
                Name = "Liliput pute",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/liliput_pute.mp3",
                Url = new Uri("https://drive.google.com/open?id=196VSd2mL_JUSeWybKejrZSWP00CZWYfn")
            },
            new Song
            {
                Name = "Qu'est ce que tu vas faire",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/qu_est_ce_que_tu_vas_faire.mp3",
                Url = new Uri("https://drive.google.com/open?id=1iOH4g1jpZm28CYoRCbWwx0qJDKCkaDDj")
            },
            new Song
            {
                Name = "Capture à la pokeball",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/capture_a_la_pokeball.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VidOg8Ho3rqkR3ltwfZNxERqtmXIQCd5")
            },
            new Song
            {
                Name = "CT c'était",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/CT_c_etait.mp3",
                Url = new Uri("https://drive.google.com/open?id=1-nCiZjqwksCMr4lYSKfLvU0Wh6VFVEkL")
            },
            new Song
            {
                Name = "Et paf",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/et_paf.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tZGTjyWgznmAFh0vElwbOEFktC4CECaB")
            },
            new Song
            {
                Name = "Non il a le pptkbla",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/non_il_a_le_pptkbla.mp3",
                Url = new Uri("https://drive.google.com/open?id=1DeBofWhagrimsUkA4zrNSMZq1bi1oW85")
            },
            new Song
            {
                Name = "Oh oui des repousses",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/oh_oui_des_repousses.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ILcFZWE4brIevXihTFJrcZBe_m57D9Rp")
            },
            new Song
            {
                Name = "Allez tue tue",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/aller_tue_tue.mp3",
                Url = new Uri("https://drive.google.com/open?id=19upTx1SaMIE9N0ADSwqyHlsXf3BoY21G")
            },
            new Song
            {
                Name = "La vie c'est pas du salami",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/la_vie_c_est_pas_du_salami.mp3",
                Url = new Uri("https://drive.google.com/open?id=1PsQMlw7UZLhnlAaKsHrvoZiCfmZQqpef")
            },
            new Song
            {
                Name = "C'est la mort qui t'as assasiné",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/c_est_la_mort_qui_t_as_assasine.mp3",
                Url = new Uri("https://drive.google.com/open?id=1m7px70XlCWKRhIvF-HIbfFfKE70-BQY3")
            },
            new Song
            {
                Name = "Ça me saoule déjà l'anus",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/ca_me_saoule_deja_l_anus.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aGwZoX9p3ujYG9bpqT47NPlHPrXeY33m")
            },
            new Song
            {
                Name = "Canardrong",
                Image = ImagePokemonMindCrystal,
                Category = CategoryImitation,
                Mp3 = "MindCrystal/Canardrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=19PY_vfGHA08BSyJJlWxiT1I0CrN4NVWN")
            },
            new Song
            {
                Name = "J'ai même pas perdu 1 pv",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/j_ai_meme_pas_perdu_1_pv.mp3",
                Url = new Uri("https://drive.google.com/open?id=16ii422d1Ts3PWlmr9YwUjcbV6tzFlRYP")
            },
            new Song
            {
                Name = "Chance de freeze",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/chance_de_freeze.mp3",
                Url = new Uri("https://drive.google.com/open?id=1R4V1ZFpwW10irmEv_psTzr4z_-SPeYsf")
            },
            new Song
            {
                Name = "Est ce que je suis con",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/Est_ce_que_je_suis_con.mp3",
                Url = new Uri("https://drive.google.com/open?id=1yTm5C63kyTulpqaXFkQOv17zYLYxSvQT")
            },
            new Song
            {
                Name = "Je me fais gang bang",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/je_me_fais_gang_bang.mp3",
                Url = new Uri("https://drive.google.com/open?id=1sXTG6fMrGJRizoNV5bjZ1aQcZL9dyUmM")
            },
            new Song
            {
                Name = "Pif paf pouf",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/pif_paf_pouf.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MlPsmOp4KU7XxVbdKFYy_YwuYujYN3_-")
            },
            new Song
            {
                Name = "Psychokwakwak",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/Psychokwakwak.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jI_vqBsViZDolUlH_rdmx6jS4rOeSeAk")
            },
            new Song
            {
                Name = "Tank tank",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/tank_tank.mp3",
                Url = new Uri("https://drive.google.com/open?id=12ju-KBkdm0Mh4sPy09NA2uzyk3Nf398h")
            },
            new Song
            {
                Name = "Abusé",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/abuse.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Jnnj_tcBGE-II-baBOyTdlww-X6PM9r9")
            },
            new Song
            {
                Name = "Eh bah alors",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/eh_bah_alors.mp3",
                Url = new Uri("https://drive.google.com/open?id=19w1KpWp7ejz6edwlxU1oSKAvJOrNe3Sm")
            },
            new Song
            {
                Name = "Il m'a brulé",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/il_m_a_brule.mp3",
                Url = new Uri("https://drive.google.com/open?id=16lNPsMbpGjvF4niYb2aB3POXgSvgJqA0")
            },
            new Song
            {
                Name = "Mais j'arrive jamais",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/mais_j_arrive_jamais.mp3",
                Url = new Uri("https://drive.google.com/open?id=1M5pgmHj9-WaVhauaBimY9yMpwSzufnrS")
            },
            new Song
            {
                Name = "Le jeu le plus dur",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/le_jeu_le_plus_dur.mp3",
                Url = new Uri("https://drive.google.com/open?id=14coGVnNiwf2-iRplMnEznu3JdI6SnNBd")
            },
            new Song
            {
                Name = "On pp stall",
                Image = ImagePokemonMindCrystal,
                Category = CategoryLivePokemonMindCrystal,
                Mp3 = "MindCrystal/on_pp_stall.mp3",
                Url = new Uri("https://drive.google.com/open?id=14x8tM6Fy84S4lNNOBa0DWGrMdJ3Y2AaP")
            },
            #endregion
            #region Live Pokemon Emeraude Full Random FAIT
            new Song
            {
                Name = "Ahahah bandes d'enfoirés",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Ahahah_bandes_denfoires.mp3",
                Url = new Uri("https://drive.google.com/open?id=14pShR27ahz4HiCLV4fEyoYiHcUNCR1oQ")
            },
            new Song
            {
                Name = "Dab dab dab",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Dab_dab_dab.mp3",
                Url = new Uri("https://drive.google.com/open?id=1335PsNKYPQKVwvUnDkwW_f8ds4jsRM8b")
            },
            new Song
            {
                Name = "Non j'ai perdu le nuzlocke",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Non_jai_perdu_le_nuzlocke.mp3",
                Url = new Uri("https://drive.google.com/open?id=1F_qlqMnH2RGwkG02or0qAO9PN3ZDcdsp")
            },
            new Song
            {
                Name = "Non putain",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Non_putain.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rgMvlSY6EcuqR9gNNy3bHMkuPlwnuS61")
            },
            new Song
            {
                Name = "Mais laissez moi vivre",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_laissez_moi_vivre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1JxuKjQMa-O5mZYRFTm28lv1DLFiRXQEy")
            },
            new Song
            {
                Name = "Moi je prends les baies et je me casse",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Moi_je_prends_les_baies_et_je_me_casse.mp3",
                Url = new Uri("https://drive.google.com/open?id=1o_nNV_1ZAIkKICJQe8UX4g3bSPrrcqin")
            },
            new Song
            {
                Name = "Rage salée",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Rage_salee.mp3",
                Url = new Uri("https://drive.google.com/open?id=18gFknpAJmqk52rcQOQzDBiOJEnb24XSC")
            },
            new Song
            {
                Name = "Respire fildrong respire",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Respire_fildrong_respire.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lUE_dR6m2oymY_f75NSGjqCkbHVCvnCb")
            },
            new Song
            {
                Name = "A watatatatata",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/A_watatatatata.mp3",
                Url = new Uri("https://drive.google.com/open?id=16XILLg9lEd1ac0E2A-ETg6nODWqLByhm")
            },
            new Song
            {
                Name = "Aaaah ahahah",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Aaaah_ahahah.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ZDFYKUpkrMmzGB3fdmSzlkMbUCKMQRqX")
            },
            new Song
            {
                Name = "Aeroblast stabb",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Aeroblast_stabb.mp3",
                Url = new Uri("https://drive.google.com/open?id=1iRW-7urSpqjRNWaiV7abP3XnnVHrcd5U")
            },
            new Song
            {
                Name = "Au fond de ta mère",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Au_fond_de_ta_mere.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mQGQoIuSItCYxRBVrdkJduw0FXKgEzNr")
            },
            new Song
            {
                Name = "Bonne nuit les amis",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Bonne_nuit_les_amis.mp3",
                Url = new Uri("https://drive.google.com/open?id=1818nS_VgqdmvmEiixBOqHHwFpbMY9fAv")
            },
            new Song
            {
                Name = "Il a feu sacré",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Il_a_feu_sacre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KY5SvLIz1ynyZCbvrpCAv00pW1W--Yvh")
            },
            new Song
            {
                Name = "Je me masturbe sur ce pokemon",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Je_me_masturbe_sur_ce_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1OJxaHmMlU7LeTXex2K0-8hM_fbhP7Ftd")
            },
            new Song
            {
                Name = "Jouissance drakhaus",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Jouissance_drakhaus.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rc-H6iN_swvy2LYbhg6cJNQo_Y_DZMcP")
            },
            new Song
            {
                Name = "ROUCOOPS",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryImitation,
                Mp3 = "EmeraudeFullRandom/ROUCOOPS.mp3",
                Url = new Uri("https://drive.google.com/open?id=1PcCadno8xI_jvlPgJ0GemM0am0mGH5z2")
            },
            new Song
            {
                Name = "What the fuck",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/What_the_fuck.mp3",
                Url = new Uri("https://drive.google.com/open?id=1RAvFtGJOwTj8u6K9pBN9t5RGinxcNfkO")
            },
            new Song
            {
                Name = "A paris y a beaucoup d'accidents",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/A_paris_ya_beaucoup_daccidents.mp3",
                Url = new Uri("https://drive.google.com/open?id=1PBLGvjbDSxC3UXZ_fZv0J_iUXprn5uGm")
            },
            new Song
            {
                Name = "Mabite est un peu dangereuse",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mabite_est_un_peu_dangereuse.mp3",
                Url = new Uri("https://drive.google.com/open?id=1HOOpAcyNtkETafY7BzyVPVra49NWvXdN")
            },
            new Song
            {
                Name = "Oh non scorplane shiny",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oh_non_scorplane_shiny.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MhfBgUEFoLQHEgzXilOu9OLKpzrPQsUF")
            },
            new Song
            {
                Name = "OoHahaaaaaaa",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/OoHahaaaaaaa.mp3",
                Url = new Uri("https://drive.google.com/open?id=1TXRuSSXHFfBHpE07Ywp-trIldYhkSyB1")
            },
            new Song
            {
                Name = "What what the floorkizfbzk",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/What_what_the_floorkizfbzk.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MhWtyIOC2zjaZCGP6LHN9XTJi0SpV22O")
            },
            new Song
            {
                Name = "Aaaahh non qu'est ce que j'ai fait",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Aaaahh_non_quest_ce_que_jai_fait.mp3",
                Url = new Uri("https://drive.google.com/open?id=14rrONvvCTHbCxif98zyHQ2m3or4Kda1X")
            },
            new Song
            {
                Name = "Argent Voilà ce que m'inspire pokemon",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Argent_Voila_ce_que_minspire_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jRDoJNGLe7RUALWC7TRfblH2SJ4y0EyQ")
            },
            new Song
            {
                Name = "Oh lui il est shiny ou je m'appelle pas fildrong",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oh_lui_il_est_shiny_ou_je_mappelle_pas_fildrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1FBJ-MBK6cE_D4gAS4a18hnGfcAZowUWL")
            },
            new Song
            {
                Name = "Piloouu",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Piloouu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mxalrQkcd_2zytzAANIYQrjNYhfTg0eO")
            },
            new Song
            {
                Name = "Plus de potions",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Plus_de_potions.mp3",
                Url = new Uri("https://drive.google.com/open?id=1I2R0_qh1LEgW0l8zQ3RwG_WQSYzo9HY3")
            },
            new Song
            {
                Name = "T'es un peu con",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Tes_un_peu_con.mp3",
                Url = new Uri("https://drive.google.com/open?id=1cXn1PmwQ5WhYtxCdw7W7GHz9T4NcAapj")
            },
            new Song
            {
                Name = "ARGENT",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/ARGENT.mp3",
                Url = new Uri("https://drive.google.com/open?id=1yyqT8bk3PrbGLudEtNTWaMDEoZLiQ2Wk")
            },
            new Song
            {
                Name = "Celebi dans les fleurs",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Celebi_dans_les_fleurs.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tqGVe79Sa6clawZHNr4J0C3FTnFdavLT")
            },
            new Song
            {
                Name = "Fifou clash",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Fifou_clash.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qpqpq_0nLmj0FqwiNL2bLS5HZlJI8L0l")
            },
            new Song
            {
                Name = "Fildrong se ruine au casino",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Fildrong_se_ruine_au_casino.mp3",
                Url = new Uri("https://drive.google.com/open?id=11Y-Y8tFAbtbHQx1cJuiF6AXsrKqRmAoy")
            },
            new Song
            {
                Name = "Fils de puuuuuuuuut",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Fils_de_puuuuuuuuut.mp3",
                Url = new Uri("https://drive.google.com/open?id=132mn2GhlWP3us10_YFj-PF1mcW7ZYXjV")
            },
            new Song
            {
                Name = "L'héroïne c'est cool",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Lheroine_cest_cool.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YQsFRRkG8R4-Wuc8z6d8f_glu0cxvYCE")
            },
            new Song
            {
                Name = "NON MON AIRMURE",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/NON_MON_AIRMURE.mp3",
                Url = new Uri("https://drive.google.com/open?id=17DpRpUamU4CIuUDrELDG1TADVYrJihpv")
            },
            new Song
            {
                Name = "Oh il a retire son pokemon",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oh_il_a_retire_son_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1g_tHJU0_5frIBD5wcp3QU9FAokoqhl_a")
            },
            new Song
            {
                Name = "Pouhaou",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Pouhaou.mp3",
                Url = new Uri("https://drive.google.com/open?id=116Xvw33QJ_T6jE77GwargFnaw0hxuL-E")
            },
            new Song
            {
                Name = "Tutututuu",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Tutututuu.mp3",
                Url = new Uri("https://drive.google.com/open?id=16en_n6SCnuMt4_lMRy0Jim5sqWJC4-UG")
            },
            new Song
            {
                Name = "Fou rire",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Fou_rire.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ITSa3tvlnctDZ-oJ1q9cFxcZAUTih2p6")
            },
            new Song
            {
                Name = "J'ai oublié mon caleçon",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Jai_oublie_mon_calecon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1AZTml9ZFhGmxXtuUPFP5Ugi2TCc5tm22")
            },
            new Song
            {
                Name = "Magby magbite",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Magby_magbite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1u6sBBrL_x4IaHeXv88KzNcO9Oz_1V1xj")
            },
            new Song
            {
                Name = "Oulala",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oulala.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SMe_o_qrmk1xtJxG1jcMExUKqFVA6arR")
            },
            new Song
            {
                Name = "Pourquoi tu tournes la tête",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Pk_tu_tournes_la_tete_salope.mp3",
                Url = new Uri("https://drive.google.com/open?id=10i0irqKnxAquczaZsyWh-9deIyc5sCif")
            },
            new Song
            {
                Name = "Si on pêche dans les fleurs on a celebi",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Si_on_peche_dans_les_fleurs_on_a_celebi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1gmDSnsinPAxNmTdEL5h1S0a93f_9s0dm")
            },
            new Song
            {
                Name = "Bonjour papa je viens te defoncer",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Bonjour_papa_je_viens_te_defoncer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1TXB2dki2S87nGElDZ6SaTmOKvYIo9ZZv")
            },
            new Song
            {
                Name = "Jpeux pas jpeux jpeux pas hein",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Jpeuxpasjpeuxjpeuxpas_hein.mp3",
                Url = new Uri("https://drive.google.com/open?id=141j5lFRjWd176cQGTzV7HF9YNS5grh0t")
            },
            new Song
            {
                Name = "Mais putain mais mes doigts..",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_putain_mais_mes_doigts...mp3",
                Url = new Uri("https://drive.google.com/open?id=1IpKHXR29IkQEaBVMss9R_YgB71OkfIqQ")
            },
            new Song
            {
                Name = "Non putain pourquoi h4iezhr8ebevdj",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Non_putain_pourquoi_h4iezhr8ebevdj.mp3",
                Url = new Uri("https://drive.google.com/open?id=1E6Utpx3rlaHsjd-t1KNSaYxJYpYP2rhZ")
            },
            new Song
            {
                Name = "On va sfaire chier sur une étoile...",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/On_va_sfaire_chier_sur_une_etoile....mp3",
                Url = new Uri("https://drive.google.com/open?id=1sVryyjCirmOoSMuQcYpWby-kfrgD3QUo")
            },
            new Song
            {
                Name = "Teuglifeuh",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Teuglifeuh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SEUroOReyLSRQLSbPns6PWXe3wCu9SNt")
            },
            new Song
            {
                Name = "Tin mais quel fils de chieeeenn",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Tin_mais_quel_fils_de_chieeeenn.mp3",
                Url = new Uri("https://drive.google.com/open?id=1P8cwCxaxeCwyKca-DjbTTpDXRY0LyL-h")
            },
            new Song
            {
                Name = "Depuis quand y a un locklass ici",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Depuis_quand_ya_un_locklass_ici.mp3",
                Url = new Uri("https://drive.google.com/open?id=1yz9LpEV8OL-o3Ryapt7JQoNxgDcUffg9")
            },
            new Song
            {
                Name = "Il a raté 3 attaques de suite",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Il_a_rate_3_attaques_de_suite_ce_connard.mp3",
                Url = new Uri("https://drive.google.com/open?id=1K5N7AIENaM2RXyOwfD_kALBV5Ocysj_j")
            },
            new Song
            {
                Name = "Krkrrkkrkrk",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Krkrrkkrkrk.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qytF83RBN1Ab7qmFJX2lFj-xN9poVfD5")
            },
            new Song
            {
                Name = "L'histoire de groudon coup de boue et cadeau",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Lhistoire_de_groudon_coup_de_boue_et_cadeau.mp3",
                Url = new Uri("https://drive.google.com/open?id=1h6l4YmMIBmT2xGppZmgV3vWIg898CcZL")
            },
            new Song
            {
                Name = "Mais c'est une blague",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_cest_une_blague.mp3",
                Url = new Uri("https://drive.google.com/open?id=1bClZR8uxZxm9tDL-iX9-njpWzW7B-jSY")
            },
            new Song
            {
                Name = "Nan mais putain de merde il s'est détruit haha",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Nan_mais_putain_de_merde_il_sest_detruit_haha.mp3",
                Url = new Uri("https://drive.google.com/open?id=1EjUsjhdPl27amSMQlMpQBceomAVo6Ktr")
            },
            new Song
            {
                Name = "Oh un Noctali mais c'est tellement bof",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oh_un_noctali_mais_cest_tellement_bof.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Gu3BBG-3S66f7Lb40aL6Et4pH-7BBzLU")
            },
            new Song
            {
                Name = "Oui euh faiteuh des doneuh et abonnez vous...",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oui_euh_faiteuh_des_doneuh_et_abonnez_vous....mp3",
                Url = new Uri("https://drive.google.com/open?id=1NmqXu5jCho-OhvhRXEOdkXXXenrJtVLV")
            },
            new Song
            {
                Name = "Putain de merde de mongol de jeu de ta mère là",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Putain_de_merde_de_mongol_de_jeu_de_ta_mere_la.mp3",
                Url = new Uri("https://drive.google.com/open?id=1NljkE4NT16jl4urCvxF4wuWG-3RQTfIB")
            },
            new Song
            {
                Name = "Rage locklass cyclone",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Rage_locklass_cyclone.mp3",
                Url = new Uri("https://drive.google.com/open?id=1y1ifdeVpOZiG0Fr4psWIxT1ex6FihqEf")
            },
            new Song
            {
                Name = "OOOOOOOHHHHAHHHHHHHH",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/OOOOOOOHHHHAHHHHHHHH.mp3",
                Url = new Uri("https://drive.google.com/open?id=1CO2emjC0-rQSLMY4K00BwZDZ2kwOgyA_")
            },
            new Song
            {
                Name = "Va niquer ta race toi",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Va_niquer_ta_race_toi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1P6R5BLIC29eK-AhfVMXOZpQVhxn-HFy9")
            },
            new Song
            {
                Name = "C'est vraiment de la merde le parc safari",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Cest_vraiment_de_la_merde_le_parc_safari.mp3",
                Url = new Uri("https://drive.google.com/open?id=17eatpm6zr7x9G6x4vc2fxrwPN9akau4i")
            },
            new Song
            {
                Name = "Danger public à velo",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Danger_public_a_velo.mp3",
                Url = new Uri("https://drive.google.com/open?id=14DGbqG9stFphP8ibqIKQXuRFLjPMo1tr")
            },
            new Song
            {
                Name = "Ma team est trop forteeeee",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Ma_team_est_trop_forteeeee.mp3",
                Url = new Uri("https://drive.google.com/open?id=1sC2Y4jvzWH52m9ZwagbJjcCY_a6WuNJv")
            },
            new Song
            {
                Name = "Oh non la beautée de souzestim ne plait pas",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oh_non_la_beautee_de_souzestim_ne_plait_pas.mp3",
                Url = new Uri("https://drive.google.com/open?id=17IB3xPpPhsUJdJ60HknnmAd-1iHGHEyy")
            },
            new Song
            {
                Name = "Aie aie aie ça fait tellement mal",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Aie_aie_aie_ca_fait_tellement_mal.mp3",
                Url = new Uri("https://drive.google.com/open?id=1F4sH7DHpDE2iCxkfaowuqVswF_Q5pH-n")
            },
            new Song
            {
                Name = "Faut aller à Atalananapolis",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Faut_aller_a_atalananapolis.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MnL6XprboAugqrXDRXp0GqC8EEX17ZgR")
            },
            new Song
            {
                Name = "Il a blizzard",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Il_a_blizzard.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Y-kaQ3QquV-iCOaDuKhzImbHmmp4TRYc")
            },
            new Song
            {
                Name = "Jouissance Minidraco",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Jouissance_minidraco.mp3",
                Url = new Uri("https://drive.google.com/open?id=18ys5OumTLi5UKitm0DzyYEyHGnsvV2gI")
            },
            new Song
            {
                Name = "L'histoire de Rayquaza",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Lhistoire_de_rayquaza.mp3",
                Url = new Uri("https://drive.google.com/open?id=1kmttOLtGXt2eeyR8GhNr_FaFGu9c-zzz")
            },
            new Song
            {
                Name = "Mais non mais j'ai déjà",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_non_mais_jai_deja.mp3",
                Url = new Uri("https://drive.google.com/open?id=1X9XourwIhs-CsbIzqOVDT7nZxoqWAstr")
            },
            new Song
            {
                Name = "Mais putain mais kdkdbebdfnrbdkfk",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_putain_mais_kdkdbebdfnrbdkfk.mp3",
                Url = new Uri("https://drive.google.com/open?id=10DwApYuP07k6_8muEDkAK2OaU09adVGk")
            },
            new Song
            {
                Name = "Manu t dur",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Manu_t_dur.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YXcFUReDDuO4h_ph-4sEEVEGpy3lcUu3")
            },
            new Song
            {
                Name = "T'as craqué ton string avec ta super potion",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Tas_craque_ton_string_avec_ta_super_potion.mp3",
                Url = new Uri("https://drive.google.com/open?id=1M7xnVKYBzJLCjcLHOUykinGBs6r48_8R")
            },
            new Song
            {
                Name = "Mais qu'est ce que j'ai fait",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_keske_jai_fait.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Muqz-2uRUvhqghApTZeL-QZSj0hi0JvI")
            },
            new Song
            {
                Name = "Mais putain de mongoldrong",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Mais_putain_de_mongoldrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ZrdxIJX1ULPyoyVuKbxmTYRDVhz-9Z2r")
            },
            new Song
            {
                Name = "Oooh j'étais venu voir Fildrong rager",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Oooh_jetais_venu_voir_fildrong_rager.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lH-joKjh-mfNdzZOQ78whZm29g8o7_Oh")
            },
            new Song
            {
                Name = "Aie aie aie reste",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Aie_aie_aie_reste.mp3",
                Url = new Uri("https://drive.google.com/open?id=1GgV4qmetfr5uD3p6DCQeWnmV1MhCTgnM")
            },
            new Song
            {
                Name = "Baie kika tiens",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/baie_kika_tiens.mp3",
                Url = new Uri("https://drive.google.com/open?id=1u-AWN_LraSnnj0jZfMk159A8WTV2TlHC")
            },
            new Song
            {
                Name = "NOOOOOOOON",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/NOOOOOOOON.mp3",
                Url = new Uri("https://drive.google.com/open?id=1DHj2vZpwd1Euzf8mwNSB_eysmQ7V0pVE")
            },
            new Song
            {
                Name = "Yes j'ai battu emeraude voilà",
                Image = ImagePokemonEmeraudeFullRandom,
                Category = CategoryLivePokemonEmeraudeFullRandom,
                Mp3 = "EmeraudeFullRandom/Yes_jai_battu_emeraude_voila.mp3",
                Url = new Uri("https://drive.google.com/open?id=1c5LD377vgc-W6ZNO8pUUol1sy4A7FKiv")
            },

            #endregion

            #region Live Pokemon Ultra Soleil FAIT
            new Song
            {
                Name = "Coup de pied dans un petit chien",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Coup_de_pied_dans_un_petit_chien.mp3",
                Url = new Uri("https://drive.google.com/open?id=1bGS5FnGND_3w0paFI5tSnJO7i85cd-qH")
            },
            new Song
            {
                Name = "Crasse cardiaque",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Crasse_cardiaque.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qOH92cQL9FIOQWN5j7HW1SAyuyXEu5vW")
            },
            new Song
            {
                Name = "Don de 300€",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Don_de_300.mp3",
                Url = new Uri("https://drive.google.com/open?id=1eeuIrCjcHiea9SfZpc-ZUGV8C5W5AIeM")
            },
            new Song
            {
                Name = "Floramentis tu vas en baver",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Floramentis_tu_vas_en_baver.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xo_u2KK1v97OME_GJC2UyeGSclWlwCvU")
            },
            new Song
            {
                Name = "J'ai raté 4 attaques",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/j_ai_rate_4_attaques.mp3",
                Url = new Uri("https://drive.google.com/open?id=1nVzOZzMoqcjavz8LD-1_U53ZLwWiH_Oz")
            },
            new Song
            {
                Name = "Mais keskecestkecetruk",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Mais_keskecestkecetruk.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YGGwfC_PUWj9fSIj9O571SnANjLdUp3G")
            },
            new Song
            {
                Name = "Muscledrong",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Muscledrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1bH6o1sM1tkyhMhZWVUMqMjMaK3WO94Lf")
            },
            new Song
            {
                Name = "Oh non mon nuzlocke",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Oh_non_mon_nuzlocke.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uVNwFcdZ11f-kBsgsTmSWZLr5oV2TTHj")
            },
            new Song
            {
                Name = "Ouhdoudoudoudou",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Ouhdoudoudoudou.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ItoSBmduU_SApcApW1NL3yc39y1xHPIv")
            },
            new Song
            {
                Name = "Salope qui counterteam",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Salope_qui_counterteam.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YOkj160ncayr6Oo771t27VqaUAqgwRK9")
            },
            new Song
            {
                Name = "Tiens ossatueur pauvre petit chaton",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Tiens_ossatueur_pauvre_petit_chaton.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aUf83bHfpb6PYDvUjWGh0u0pB5d8V-Aa")
            },
            new Song
            {
                Name = "Adieu...",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Adieu....mp3",
                Url = new Uri("https://drive.google.com/open?id=1JScfMdvN0XMwUlX0IkpiwbBSMYNuPL4Y")
            },
            new Song
            {
                Name = "Coup critique non fulgence",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Coup_critique_non_fulgence.mp3",
                Url = new Uri("https://drive.google.com/open?id=11fVPuYpYW4uOoVZmH7jSM7rdq6KiGBuT")
            },
            new Song
            {
                Name = "Coup critique non Magnifique nan",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Coup_critique_non_Magnifique_nan.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mx-3zQ4GENjTr-rNyFnl14ocCZvZ2l0h")
            },
            new Song
            {
                Name = "Dans la chatte à ta mère grotadmorv",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Dans_la_chatte_a_ta_mere_grotadmorv.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Bptc_5pa2uwoqDDrCxWUDiQk2Nye9ofx")
            },
            new Song
            {
                Name = "Et voilà voilà vous êtes content",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Et_voila_voila_vous_etes_content.mp3",
                Url = new Uri("https://drive.google.com/open?id=1pm0GBUQIV7PVt9IwgUi5mXJWng2FcdYx")
            },
            new Song
            {
                Name = "Necrozma non il a bouffe lunala enfin le jeu...",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Necrozma_non_il_a_bouffe_lunala_enfin_le_jeu....mp3",
                Url = new Uri("https://drive.google.com/open?id=1I5wxtIyDYrJvxcS_7haGyb6WRNMD5ut-")
            },
            new Song
            {
                Name = "Necrozma voilà c'est vos mères qui perdent",
                Image = ImagePokemonUltraSoleil,
                Category = CategoryLivePokemonUltraSoleil,
                Mp3 = "UltraSoleil/Necrozma_voila_cest_vos_meres_qui_perdent.mp3",
                Url = new Uri("https://drive.google.com/open?id=1knQSvZVtRmUuDOGvFn8zz3W3rNA1t2oe")
            },

            #endregion
            #region Live Pokemon Vega FAIT
            new Song
            {
                Name = "Ah ah ah ah ah",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/ah_ah_ah_ah_ah.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SGHY7sYN_yKLWOpDa2gaT-C-XlQbeAmn")
            },
            new Song
            {
                Name = "AH AH OH PUTAIN NON QUOI",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/AH_AH_OH_PUTAIN_NON_QUOI.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MmWYXdGNb5W_d_FicCig7BRZB5Mh7vju")
            },
            new Song
            {
                Name = "Arrête de flinch mes Pokemons",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/arrete_de_flinch_mes_pokemons.mp3",
                Url = new Uri("https://drive.google.com/open?id=12PJLrpL9u32MKEp2KdpyaHz5cFopuQc5")
            },
            new Song
            {
                Name = "Att attend attend",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/att_attend_attend.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IC7UIpjAKVdVnyuFtFf_FiFE2vu5hg0V")
            },
            new Song
            {
                Name = "Capturer",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/capturer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1flLGgHVgxzzRBCwNHoJpjHqnFyL2He3S")
            },
            new Song
            {
                Name = "Comment tagrossemere évolue",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/comment_tagrossemere_evolue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1zoicyuOZyaRWlPZ3tswOicYF-FXBjaGO")
            },
            new Song
            {
                Name = "Déjà des dresseurs",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/deja_des_dresseurs.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vx1DhK7pEP-4OSz1Gf3_QnpkvmGfHA_6")
            },
            new Song
            {
                Name = "Eeeeeeeeeeeeeeeeeeeeeh",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/eeeeeeeeeeeeeeeeeeeeeh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mdU3A53KcA-HyZOs5z6tcOsPU3a77xws")
            },
            new Song
            {
                Name = "Non il m'a",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/non_il_m_a.mp3",
                Url = new Uri("https://drive.google.com/open?id=1VBxRK8Vjpis8gQag8kWS6fxoQe7slnNG")
            },
            new Song
            {
                Name = "Oh j'ai un papa",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/oh_j_ai_un_papa.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hBsIBmxsB1qd12CAxXUw8qRhlFj7hvL6")
            },
            new Song
            {
                Name = "Oh mon dieu il est énervé",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/oh_mon_dieu_il_est_enerve.mp3",
                Url = new Uri("https://drive.google.com/open?id=1S1xWbN6eE_L-_DMyS3NGVPfUGY1gf0mf")
            },
            new Song
            {
                Name = "Oh oh oh oh",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/oh_oh_oh_oh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qcVKsVW-QQjvTEGn-kJQeCvcNrMM7c26")
            },
            new Song
            {
                Name = "Oh oui oui oui oui",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/oh_oui_oui_oui_oui.mp3",
                Url = new Uri("https://drive.google.com/open?id=1BnzamvRgxjrzleqOlSxs87XtfIB0S88j")
            },
            new Song
            {
                Name = "On veut choisir un pokemon",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/on_veut_choisir_un_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1d4l8422KJUeTVE26WbQQu0ihT6uLC-5H")
            },
            new Song
            {
                Name = "Putain ta mère la schneck",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/putain_ta_mere_la_schneck.mp3",
                Url = new Uri("https://drive.google.com/open?id=1O5Nz1ZkvSKI_pnzxF9o_0YKQXBu-cglc")
            },
            new Song
            {
                Name = "Qu'est ce que c'est que ce truc",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/qu_est_ce_que_c_est_que_ce_truc.mp3",
                Url = new Uri("https://drive.google.com/open?id=16fKTUE57nxHYtSzGKUtJYnvIO99NSD0W")
            },
            new Song
            {
                Name = "Tagrossemere a appris charme",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/tagrossemere_a_appris_charme.mp3",
                Url = new Uri("https://drive.google.com/open?id=1sMViXuoEaeFWlynNjoskj6ZaUM8i7AtU")
            },
            new Song
            {
                Name = "Jle touche pas",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/jle_touche_pas.mp3",
                Url = new Uri("https://drive.google.com/open?id=1zo0xSsbk6cWNg5C19OOuVL4lSanZp9xc")
            },
            new Song
            {
                Name = "Lalalalalalalalalalalalala",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/lalalalalalalalalalalalala.mp3",
                Url = new Uri("https://drive.google.com/open?id=1tFbJpTUzRlkXgNubTwF5rT5Oc-WBoy89")
            },
            new Song
            {
                Name = "Oh non un seisme",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/oh_non_un_seisme.mp3",
                Url = new Uri("https://drive.google.com/open?id=1GO7mpbPiiXeHT3olw-f__Tb-MnY9bOWE")
            },
            new Song
            {
                Name = "Mais pute pute pute",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/mais_pute_pute_pute.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aUANbFsMG12iVvv5mN5nsiGMFbc2o_F_")
            },
            new Song
            {
                Name = "Utilise la mort de pour soigner",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/utilise_la_mort_de_pour_soigner.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Xyvq4Q6JFyqsoiFysDdLOU_b8y0pPj4f")
            },
            new Song
            {
                Name = "Ça me saoule la bite",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/ca_me_saoule_la_bite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xEPZNJFqNTuo9BwIEoLkZyHffZFZ1mDZ")
            },
            new Song
            {
                Name = "C'est nul marrant bizarre",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/cest_nul_marrant_bizarre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MYiHMS54__PTEHHNZ7Aa3cKsh_ASGc7f")
            },
            new Song
            {
                Name = "Ah ouais nan mais c'est génant",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/ah_ouais_nan_mais_c_est_genant.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Q7RXkXlcf-LcjUq99xq7uf_bxRiBcBGc")
            },
            new Song
            {
                Name = "Il a tank ça comment",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/il_a_tank_ca_comment.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lL3pgBu_97zLfRptE7Nop5y8V5uzk61P")
            },
            new Song
            {
                Name = "Je sais pas ou on doit aller",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/je_sais_pas_ou_on_doit_aller.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ebBBZn0LePZR8ldjIVAVCOJKaZgOODaL")
            },
            new Song
            {
                Name = "Magmortar en retard",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/magmortar_en_retard.mp3",
                Url = new Uri("https://drive.google.com/open?id=1fgahaWRhzyjSnWb4sp9K2E5u5W2qG63h")
            },
            new Song
            {
                Name = "Monique et l'oncle filibert",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/monique_et_l_oncle_filibert.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aQFyU7FQegqMrtHU4JnhdwMx6rEzydIt")
            },
            new Song
            {
                Name = "Non c'est pas fini",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/non_c_est_pas_fini.mp3",
                Url = new Uri("https://drive.google.com/open?id=17OfD5V8blG5MgoFyAJkwus6CLGP0kew_")
            },
            new Song
            {
                Name = "Story mewtwo",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/story_mewtwo.mp3",
                Url = new Uri("https://drive.google.com/open?id=13L4NAD15wU5lJlw6-vYxGq1oA2y1mCJv")
            },
            new Song
            {
                Name = "Ballondrong",
                Image = ImagePokemonVega,
                Category = CategoryImitation,
                Mp3 = "Vega/Ballondrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=14e_EfCRQgcriaiDcDfDbofMkgUTBUd6C")
            },
            new Song
            {
                Name = "C'est parti pour le sweep",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/c_est_parti_pour_le_sweep.mp3",
                Url = new Uri("https://drive.google.com/open?id=14Gz58YdkTTYHZxPAdBKqQeEfVdrqmb3I")
            },
            new Song
            {
                Name = "Cosmic power dududu",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/cosmic_power_dududu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xJR6QvyFewd4POB-C9qS5ef7_midMrAo")
            },
            new Song
            {
                Name = "Je crois que j'ai jamais défoncé autant une meuf",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/jamais_defonce_autant_une_meuf.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vmMeNrcQ6X0tPmx0zJ4s4cfkISXZRVSE")
            },
            new Song
            {
                Name = "Il est trop cool c'est quoi ce truc",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/il_est_trop_cool_c_est_quoi_ce_truc.mp3",
                Url = new Uri("https://drive.google.com/open?id=1truLpl_TIrhRtth8XAaG1uI5pR8t9ROg")
            },
            new Song
            {
                Name = "Il est trop mignon stylé jle veux",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/il_est_trop_mignon_style_jle_veux.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KSgPhAW7Pls6dnUgKNKwHx03I5xRblzb")
            },
            new Song
            {
                Name = "Jouissancedrong",
                Image = ImagePokemonVega,
                Category = CategoryImitation,
                Mp3 = "Vega/jouissancedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=15038CiLpybPl_vsy8CBWWCBNbyKa7rDq")
            },
            new Song
            {
                Name = "Ma team a de sacré problèmes contre ça",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/ma_team_a_de_sacre_problemes_contre_ca.mp3",
                Url = new Uri("https://drive.google.com/open?id=14ODwbKG5JOxrjvEjVjj3QXkik2ZN6kUu")
            },
            new Song
            {
                Name = "Pin pon pin pon",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/pin_pon_pin_pon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1v6KzBIARkyhES869DUMj4aVBwMBGqyMZ")
            },
            new Song
            {
                Name = "Revient monte à droite",
                Image = ImagePokemonVega,
                Category = CategoryLivePokemonVega,
                Mp3 = "Vega/revient_monte_a_droite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1AqKilpFykO2SmTltV9l2GOqEDLngMIXg")
            },
            #endregion          
            #region Live Pokemon Insurgence FAIT
            new Song
            {
                Name = "Allez pokeball go",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/allez_pokeball_go.mp3",
                Url = new Uri("https://drive.google.com/open?id=16nXDW1SyNaDTjEZHaXF2wUVw2fqFsoEf")
            },
            new Song
            {
                Name = "Ta mère",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/Ta_mere_echo.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KEznKO2Zm2Kj7Uk1vLk_lGJppQEygvuU")
            },
            new Song
            {
                Name = "Il a jamais fait de strategie",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/jamais_fait_de_strategie.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xJzAK1sqmAqO2mTix5hrKmsq1fvT02iW")
            },
            new Song
            {
                Name = "Kraknoixdrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/kraknoixdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1CUDhOqTY3Ar22QsKnm-ZbN91zxaHquA_")
            },
            new Song
            {
                Name = "Larveyettedrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/larveyettedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IMl4ENaeZZ8YQPGi_DMGZ2ihIzPPkrah")
            },
            new Song
            {
                Name = "Luxiodrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/luxiodrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1JNyHxCXtu8_b07f01go8-3kPo2udKr_E")
            },
            new Song
            {
                Name = "Macroniumdrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/macroniumdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1l9Tn6n6kB-dWVK4eIs8i8G1jHvmfXbDM")
            },
            new Song
            {
                Name = "Queue de cheval",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/queue_de_cheval.mp3",
                Url = new Uri("https://drive.google.com/open?id=1oMfLBsN-rEFDXnGRlSRC2g8fVlEqttuZ")
            },
            new Song
            {
                Name = "Team méchante",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/team_mechante.mp3",
                Url = new Uri("https://drive.google.com/open?id=19_XnfQ2vk0mDLbNotOQktks7CRHbG2I8")
            },
            new Song
            {
                Name = "Goupixdrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/goupixdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jTUbeC05e94166qNurRU9WkSnA9xuKev")
            },
            new Song
            {
                Name = "Méga évoluer",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/mega_evoluer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Wt6wL_uzw2h6TQtHsAEOlBnzHhxuHrua")
            },
            new Song
            {
                Name = "Nidokingdrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/nidokingdrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1BtF-gbfnCGnjK0aQjSwErtIdiRWGux54")
            },
            new Song
            {
                Name = "Station épuration",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/station_epuration.mp3",
                Url = new Uri("https://drive.google.com/open?id=1KN5GdkC4OkRHt4zSRnTVWL3U874_5S1T")
            },
            new Song
            {
                Name = "Anus attaque suppositoir",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/anus_attaque_suppositoir.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mrVw4b6fzr5aauXmGrwrqNIYi8eO7O0N")
            },
            new Song
            {
                Name = "Mort pas sauce piquante",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/mort_pas_sauce_piquante.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xA2b6wfjn0dMSNuA3I4Hmq4b9-e8kKYA")
            },
            new Song
            {
                Name = "Oh un dollar par terre",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/oh_un_dollar_par_terre.mp3",
                Url = new Uri("https://drive.google.com/open?id=109l5zO5S_pwnxVsXcbiHH_CbQedKQJ1w")
            },
            new Song
            {
                Name = "Pokemon cancer ultimeuh",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/pokemon_cancer_ultimeuh.mp3",
                Url = new Uri("https://drive.google.com/open?id=1oaim8ryHgA_2stmR5Jmkx9VRdxWBN--6")
            },
            new Song
            {
                Name = "Qui a fait ce sprite",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/qui_a_fait_ce_sprite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jJ-mro2qOrA37PTKYDyZGiidkYaUMb_z")
            },
            new Song
            {
                Name = "Une cuillière pour papa",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/une_cuilllere_pour_papa.mp3",
                Url = new Uri("https://drive.google.com/open?id=1dRYo7ur2pqB7Lhr0pU5A01SGjlWqiDSx")
            },
            new Song
            {
                Name = "Ahouhyhahah",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/ahouhyhahah.mp3",
                Url = new Uri("https://drive.google.com/open?id=1hTpEL7oIvjWHPGppkAri8FNvONwIpRnr")
            },
            new Song
            {
                Name = "Wow 1 000 000",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/wow_1000000.mp3",
                Url = new Uri("https://drive.google.com/open?id=1AtbIEXsY4rSL2S15c5_tEbiq6jwR-0K2")
            },
            new Song
            {
                Name = "Y a un pauvre papier",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/y_a_un_pauvre_papier.mp3",
                Url = new Uri("https://drive.google.com/open?id=154L-uQGokTp-HTbtOIhOWReljSz0rFcg")
            },
            new Song
            {
                Name = "Attaque de boost",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/attaque_de_boost.mp3",
                Url = new Uri("https://drive.google.com/open?id=1WjANF9PjUjqAG5PIIly2ho4ddWJR6OLa")
            },
            new Song
            {
                Name = "Labyrinthe musique",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/labyrinthe_musique.mp3",
                Url = new Uri("https://drive.google.com/open?id=16DueBJ66o-sLPROXpS2DH-fYyHmkW67f")
            },
            new Song
            {
                Name = "Non respecte toi",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/non_respect_toi.mp3",
                Url = new Uri("https://drive.google.com/open?id=1I6EFhcGrcc7EAvb_46EE55kbgbImwr6b")
            },
            new Song
            {
                Name = "Set nul",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/set_nul.mp3",
                Url = new Uri("https://drive.google.com/open?id=1diD62PHMioLWT0wqvKkiNLUX_k6efZH4")
            },
            new Song
            {
                Name = "Alors toi tu vas rentrer dans ma pokeball",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/alors_toi_tu_vas_rentrer_dans_ma_pokeball.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vxXHCcfDOAhSAYC4Gua9wAyNzqBFJTwl")
            },
            new Song
            {
                Name = "Il me le faut vite",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/il_me_le_faut_vite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1g08nSlg0T5IrLEfcsGGfayCTUkfAKRc2")
            },
            new Song
            {
                Name = "J'ai pas de girlfriend moi",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/j_ai_pas_de_girlfriend_moi.mp3",
                Url = new Uri("https://drive.google.com/open?id=16BxYNZAGUTyARuSHk75uBJ7gS0vEAAh0")
            },
            new Song
            {
                Name = "Mais regardez le",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/mais_regardez_le.mp3",
                Url = new Uri("https://drive.google.com/open?id=1dP9G60VsQkxL8LJlEUdM48ik6C5ZsyBW")
            },
            new Song
            {
                Name = "Mais tabarnak",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/mais_tabarnak.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ZfMY1gk6oDaqll63wij2DeQ3yQleWt2k")
            },
            new Song
            {
                Name = "Non non pas ça",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/non_non_pas_ca.mp3",
                Url = new Uri("https://drive.google.com/open?id=1JoWztz8N016vLKnogwIiT_qf8Ci2XCk7")
            },
            new Song
            {
                Name = "Oh mon dieu voilà ma copine",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/oh_mon_dieu_voila_ma_copine.mp3",
                Url = new Uri("https://drive.google.com/open?id=13blJHIVQWliwfYhCFcoGtQlceRVc44JH")
            },
            new Song
            {
                Name = "WHAT WHAT",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/WHAT_WHAT.mp3",
                Url = new Uri("https://drive.google.com/open?id=1cEmJE5_FYWpDQsMnMzXH20V1T64VZkRZ")
            },
            new Song
            {
                Name = "Le niveau de cringe",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/le_niveau_de_cringe.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Raus3dNAzKceC_jGsOeFdBuASB5xcxx0")
            },
            new Song
            {
                Name = "Ce jeu me casse les burnes",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/ce_jeu_me_casse_les_burnes.mp3",
                Url = new Uri("https://drive.google.com/open?id=1HIHPR-3BqZD3OgD2T0GK86EuZl8kx6BE")
            },
            new Song
            {
                Name = "Non not fun ola ola",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/non_not_fun_ola_ola.mp3",
                Url = new Uri("https://drive.google.com/open?id=1WFGhYncdTKqxoA3BbCU6RSr__3bVGdk6")
            },
            new Song
            {
                Name = "Pouce bleu dons",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/pouce_bleu_dons.mp3",
                Url = new Uri("https://drive.google.com/open?id=17syvuZx6c6QqXOHO5cMFHGH2Zo-g8YY7")
            },
            new Song
            {
                Name = "Celebi shiny (version insurgence)",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/celebi_shiny_version_insurgence.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xYuk9GbglNR2H3S0IdWvks1fPQGm7kVZ")
            },
            new Song
            {
                Name = "DJ patrick",
                Image = ImagePokemonInsurgence,
                Category = CategoryImitation,
                Mp3 = "Insurgence/DJ_patrick.mp3",
                Url = new Uri("https://drive.google.com/open?id=1HulwV1PwIXNH0j48t3gKO0HeWwkaIJk8")
            },
            new Song
            {
                Name = "Enigmedrong",
                Image = ImagePokemonInsurgence,
                Category = CategoryLivePokemonInsurgence,
                Mp3 = "Insurgence/Enigmedrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=1RpwPqc0vV_vvk9nUm9yyy2Hh_VCrqQsS")
            },

            #endregion
            #region Live Pokemon Uranium FAIT / à compléter
            new Song
            {
                Name = "Ça va créer un Superman",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/ca_va_creer_un_superman.mp3",
                Url = new Uri("https://drive.google.com/open?id=12Vvh-cruSfp0DtJmPrGZoWRdOonPujvJ")
            },
            new Song
            {
                Name = "C'est moche",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/cest_moche.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rihohACa4VSVBS_QNJxepmAxefcrDI9i")
            },
            new Song
            {
                Name = "Elle a l'air complètement gogole",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/elle_a_lair_completement_gogole.mp3",
                Url = new Uri("https://drive.google.com/open?id=17XeWTLFc5tOTBoulRsq6zSrMAwwXSMuk")
            },
            new Song
            {
                Name = "Je déteste les grottes",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/je_deteste_les_grottes.mp3",
                Url = new Uri("https://drive.google.com/open?id=1h_h2yWf1GDh-Lx1j6gcw0oJEM3Bwza4p")
            },
            new Song
            {
                Name = "Kevlar",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/kevlar.mp3",
                Url = new Uri("https://drive.google.com/open?id=1V5Tfe9Bis6PP16bBTbCmnSq7i-1SGnTq")
            },
            new Song
            {
                Name = "Le prof",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/le_prof.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Bdlzo275xaKsEQaF3KAeWlz2SOWzA6PR")
            },
            new Song
            {
                Name = "Le ratachu",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/le_ratachu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1sbba6_ZXILI9WpCD0YCn7_Jru8gdJNqL")
            },
            new Song
            {
                Name = "Les Pokemons ont des pouvoirs",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/les_pokemons_ont_des_pouvoirs.mp3",
                Url = new Uri("https://drive.google.com/open?id=1NO71_Qlyh4twq6xwTZOYXPh-kkhTSzr2")
            },
            new Song
            {
                Name = "Théo",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/theo.mp3",
                Url = new Uri("https://drive.google.com/open?id=1MEsGpOqRwrBE5yHweUp4lLclOaFKoadX")
            },
            new Song
            {
                Name = "Grâce à patou",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/grace_a_patou.mp3",
                Url = new Uri("https://drive.google.com/open?id=12jyKKJO5QKU_jCwLMit6JWw5eUIsaiqe")
            },
            new Song
            {
                Name = "J'adore les caves",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/jadore_les_caves.mp3",
                Url = new Uri("https://drive.google.com/open?id=1SZrKMMKOKKWHP0O8mJpOFWNhE8qLhxSF")
            },
            new Song
            {
                Name = "Les fourmis rouges",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/les_fourmis_rouges.mp3",
                Url = new Uri("https://drive.google.com/open?id=1BW8kOM0eNlhtc1Ai32zxzjp06E8HIgGn")
            },
            new Song
            {
                Name = "Mais mon évolution",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/mais_mon_evolution.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IISQ1ui2_Pp25JrPgVZpmqWc0nDfpljL")
            },
            new Song
            {
                Name = "Patou comme à l'entrainement",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/patou_comme_a_lentrainement.mp3",
                Url = new Uri("https://drive.google.com/open?id=1kYKl0ogGtVslJGDmuDYqKTKY074FfZez")
            },
            new Song
            {
                Name = "Quel horreur",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/quel_horreur.mp3",
                Url = new Uri("https://drive.google.com/open?id=1lOHQW6KQaTG9EqWLXxEgzAoZUZXOyJpd")
            },
            new Song
            {
                Name = "Arreter de faire bite",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/arreter_de_faire_bite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1T_iZDkEuAH72uRuuMGp6NNTh0jngtlRd")
            },
            new Song
            {
                Name = "C'est de l'humour",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/cest_de_lhumour.mp3",
                Url = new Uri("https://drive.google.com/open?id=1DBun0z9nAN5tFwDi5TuRbCaWVDyMhZb4")
            },
            new Song
            {
                Name = "C'est horrible c'est un poisson",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/cest_horrible_cest_un_poisson.mp3",
                Url = new Uri("https://drive.google.com/open?id=1aoBHhorUlYpqIKoDMSh5kZFCXcQ8_kM-")
            },
            new Song
            {
                Name = "A demain sous le train",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/sous_le_train.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ru10OvG1x1bw7ZmLYobAvvgC_3AxJIme")
            },
            new Song
            {
                Name = "Pleure",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/fildrong_pleure.mp3",
                Url = new Uri("https://drive.google.com/open?id=1LR11ljeMh8bRBZFRqw5NATr8_0k_kMA5")
            },
            new Song
            {
                Name = "Oh non non",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/oh_non_non.mp3",
                Url = new Uri("https://drive.google.com/open?id=1BW5tR4xxwB_m6sr7YfCwjvVPlhaTPXnX")
            },
            new Song
            {
                Name = "Ça casse pas 3 pates à un canard boiteux",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/ca_casse_pas_3_pates_a_un_canard_boiteux.mp3",
                Url = new Uri("https://drive.google.com/open?id=1d-nUyK-xy9fxMv2XZ9L41BBnaqLgZruQ")
            },
            new Song
            {
                Name = "Tu es un esclave",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/tu_es_un_esclave.mp3",
                Url = new Uri("https://drive.google.com/open?id=19VYTB_Lm6gDPBN1D4Ql942aJKPKOyNWN")
            },
            new Song
            {
                Name = "Utilise bite",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/utilise_bite.mp3",
                Url = new Uri("https://drive.google.com/open?id=1IvO30pj8VANV1CjIV1yBfrzcY3e2nSI_")
            },
            new Song
            {
                Name = "J'ai envie de casser ta mère",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/jai_envie_de_casser_ta_mere.mp3",
                Url = new Uri("https://drive.google.com/open?id=1mRRJ9THdl677_u4h33jng7Ebdj3wKrqd")
            },
            new Song
            {
                Name = "Oh patou",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/oh_patou.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uiWwtxOnqvfAFyuIXugpbvkYuhBYUmmj")
            },
            new Song
            {
                Name = "Patou noon",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/patou_noon.mp3",
                Url = new Uri("https://drive.google.com/open?id=16CjMyjaHX-OJhjIVVA3CQyuvM5dX9G_A")
            },
            new Song
            {
                Name = "Ta petite soeur",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/ta_petite_soeur.mp3",
                Url = new Uri("https://drive.google.com/open?id=1PgqPyM6k3YVsu6gSKqKc5AFkPEReLZ7p")
            },
            new Song
            {
                Name = "Dans ta geule",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/dans_ta_geule.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jD6mI43dUYad2Ys7U8p4XewVqTAN7YKL")
            },
            new Song
            {
                Name = "Et puis là",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/et_puis_la.mp3",
                Url = new Uri("https://drive.google.com/open?id=1xbHcKHmaCGSq_lXn2P29Wl49NQooODW-")
            },
            new Song
            {
                Name = "T'es une merde",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/tes_une_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ZsHCu-eUmB47dyfLZZ3PXqxO7gaEYPwt")
            },
            new Song
            {
                Name = "Tu me fait me déplacer",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/tu_me_fait_me_deplacer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1G6A6W99Td-K9A382IpskVt1FCnBi7CBT")
            },
            new Song
            {
                Name = "Ça fait pas très peur",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/ca_fait_pas_tres_peur.mp3",
                Url = new Uri("https://drive.google.com/open?id=11JSYQ2nb0cAB_eRKTQDi4suq_XmeZEpK")
            },
            new Song
            {
                Name = "Connard jon-michon",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/connard_jon-michon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1-E650BjbnsyNgTaRkjnXHU_2Wxefom1M")
            },
            new Song
            {
                Name = "Le marin c'est pendu",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/le_marin_cest_pendu.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Pr7qhWwE2q2FqzeBNWnKpa54wloqpmAR")
            },
            new Song
            {
                Name = "Au revoir patou",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/au_revoir_patou.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qVAesDQeeSw0bFV7KhHb9Lt7wqt3suWD")
            },
            new Song
            {
                Name = "Il est partout",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/il_est_partout.mp3",
                Url = new Uri("https://drive.google.com/open?id=1e1eArJj8Bb50p6VeUqBg8_FITmMPPGtb")
            },
            new Song
            {
                Name = "Je te hais Théo",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/je_te_hais_theo.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Wt7jJFN7tGJXHD8bNr9SAQIUfYvPyf-N")
            },
            new Song
            {
                Name = "Occupe toi de",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/occupe_toi_de.mp3",
                Url = new Uri("https://drive.google.com/open?id=1sx7bnR5lvUZvZtkje52OlJiJPReo87JW")
            },
            new Song
            {
                Name = "Gros bébé",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/gros_bebe.mp3",
                Url = new Uri("https://drive.google.com/open?id=1H9KmkbadV13Nial5RpIre-kzFg95R7Cf")
            },
            new Song
            {
                Name = "Phalouse",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/phalouse.mp3",
                Url = new Uri("https://drive.google.com/open?id=1dQoDKMBC5c02TGAx_eU_nfvlu21jTJ75")
            },
            new Song
            {
                Name = "ASMR (1m50)",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/ASMR.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YjPGqELH6_jz55mfNlEDYpLbSgskcirO")
            },
            new Song
            {
                Name = "Des chinois surfeur",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/des_chinois_surfer.mp3",
                Url = new Uri("https://drive.google.com/open?id=13-i66L13AkxgYyz8WE1tBogPDwA5Dh5D")
            },
            new Song
            {
                Name = "ASMR keskecekca",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/ASMR_keskecekca.mp3",
                Url = new Uri("https://drive.google.com/open?id=1_oWBrUE-7GQX6R0GIWQFFZc2daP3SBm1")
            },
            new Song
            {
                Name = "c'est la merde",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/cest_la_merde.mp3",
                Url = new Uri("https://drive.google.com/open?id=11ySL0j2qRyG-bbN9ToBgVPcT43F1mFo7")
            },
            new Song
            {
                Name = "Non Théo",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/non_theo.mp3",
                Url = new Uri("https://drive.google.com/open?id=1uxy2jz9iGB7hI7xExlP0BYaGs-SQaX-v")
            },
            new Song
            {
                Name = "C'est dur pour Théo",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/cest_dur_pour_theo.mp3",
                Url = new Uri("https://drive.google.com/open?id=1c2xn6pZFzDAIzE7aPsEIkJhShffGUK03")
            },
            new Song
            {
                Name = "Il a oublié lance-flamme",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/il_a_oublie_lance_flamme.mp3",
                Url = new Uri("https://drive.google.com/open?id=1D0thIB3M5C4do7nWw6_MlynUqqBevi_L")
            },
            new Song
            {
                Name = "J'ai réservé 3 mois de haine",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/jai_reserve_3_mois_de_haine.mp3",
                Url = new Uri("https://drive.google.com/open?id=1rwyO5gJqAIAvM9qRUrOWR66f1Iq5_o3B")
            },
            new Song
            {
                Name = "Le mec look de fion",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/le_mec_look_de_fion.mp3",
                Url = new Uri("https://drive.google.com/open?id=1vM5j7i5p2haV7q3ugMeUqbSSXy1ixEBD")
            },
            new Song
            {
                Name = "Je me fais victime par la team rocket",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/je_me_fais_victime_par_la_team_rocket.mp3",
                Url = new Uri("https://drive.google.com/open?id=1y9R6ZJqHW5OjqmBd5vCIRJDPSpr8P71q")
            },
            new Song
            {
                Name = "Tabascodrong",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/tabascodrong.mp3",
                Url = new Uri("https://drive.google.com/open?id=14e6Fy-wigCAAMbplo_JaYvQ7ZkbKHSlC")
            },
            new Song
            {
                Name = "Il s'est fait couper la tête",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/il_sest_fait_couper_la_tete.mp3",
                Url = new Uri("https://drive.google.com/open?id=14m51oHZ1db4kl7O93KixXJOPEBPHGm0S")
            },
            new Song
            {
                Name = "Faut arrêter de se foutre de ma gueule",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/faut_arreter_de_se_foutre_de_ma_gueule.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qYxLQztjyZ1eU4uGiSL_C2lMZHLZmhGw")
            },
            new Song
            {
                Name = "C'est déjà assez compliqué",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/cest_deja_assez_complique.mp3",
                Url = new Uri("https://drive.google.com/open?id=1meF4N5Ti6dHurG261podseTFPmzYmrqm")
            },
            new Song
            {
                Name = "Des yeux de chinois",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/des_yeux_de_chinois.mp3",
                Url = new Uri("https://drive.google.com/open?id=1cRxIvygQEzEEcMwvwwyY0K3hRZ-pGrKB")
            },
            new Song
            {
                Name = "No-rage de mon décoltage",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/no-rage_de_mon_decoltage.mp3",
                Url = new Uri("https://drive.google.com/open?id=15g3K3vgOpg26rOwtro01MxXg9yLLSR3A")
            },
            new Song
            {
                Name = "Il y a de la lave dans le centre pokemon",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/il_y_a_de_la_lave_dans_le_centre_pokemon.mp3",
                Url = new Uri("https://drive.google.com/open?id=1wEOYLVUVgVyBzgBV7VGpC0cQHNzubf_K")
            },
            new Song
            {
                Name = "Je perds mes cheuveux",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/je_perds_mes_cheuveux.mp3",
                Url = new Uri("https://drive.google.com/open?id=1nmcPMM02yEgAHhrF84tviczR5F8SnA9h")
            },
            new Song
            {
                Name = "Je déteste pokemon uranium (petite chanson)",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/Je_deteste_pokemon_uraniumpetite_chanson.mp3",
                Url = new Uri("https://drive.google.com/open?id=1nO_-e8qxepbvUzOQfJmn73NSANM_77k_")
            },
            new Song
            {
                Name = "Grosse rage d'un game over",
                Image = ImagePokemonUranium,
                Category = CategoryLivePokemonUranium,
                Mp3 = "Uranium/grosse_rage_dun_game_over.mp3",
                Url = new Uri("https://drive.google.com/open?id=1u3zEO_dCnjH2tPGnJQPOL2JOov8SNyPK")
            },
            #endregion
            #region Musique
            new Song
            {
                Name = "Chanson de la Nouvelle Strat - Impact",
                Image = "musique_NouvelleStrat.png",
                Category = CategoryMusique,
                Mp3 = "musique/NouvelleStrat.mp3",
                Url = new Uri("https://drive.google.com/open?id=1DKbrkviVy2PFu-FRMDtDXlINNmsoXrET")
            },
            new Song
            {
                Name = "CiFR 2018",
                Image = "musique_CIFR2018.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Pokemon-CiFR-2018.mp3",
                Url = new Uri("https://drive.google.com/open?id=18KDxzwGjCuewcpOMjE8KOuNfRmhZGluo")
            },
            new Song
            {
                Name = "CiFR 2019",
                Image = "musique_CIFR2019.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Pokemon-CIFR-2019.mp3",
                Url = new Uri("https://drive.google.com/open?id=1B5ZIGscu5eZajY47e1A646mEwNlmW3hY")
            },
            new Song
            {
                Name = "CiFR 2020",
                Image = "musique_CIFR2020.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Pokemon-CIFR-2020.mp3",
                Url = new Uri("https://drive.google.com/open?id=1YF7_IRKM9ZxuSL85cOM-1ksaX6oxtUAp")
            },
            new Song
            {
                Name = "Envie de pleurer",
                Image = "musique_enviepleurer.png",
                Category = CategoryMusique,
                Mp3 = "musique/enviepleurer.mp3",
                Url = new Uri("https://drive.google.com/open?id=1t6Mv8L76YbDjAIuPu0LPYdJwzn_bSkQu")
            },
            new Song
            {
                Name = "Esprit Revanchard",
                Image = "musique_Esprit.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Esprit-Revanchard.mp3",
                Url = new Uri("https://drive.google.com/open?id=1Vmt43f0hX-uaKG2E6uZrrlITTb1-qHFo")
            },
            new Song
            {
                Name = "Jean-Hubert l'abruti",
                Image = "musique_jeanhubert.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Jean-Hubert-l_abruti.mp3",
                Url = new Uri("https://drive.google.com/open?id=1qusbb8Xbg7KQoNqzhWfmUsJEQjISrnye")
            },
            new Song
            {
                Name = "Loubards veulent se battre",
                Image = "musique_Loubard.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Loubards-veulent-se-battre.mp3",
                Url = new Uri("https://drive.google.com/open?id=1F_3hwIQp6kEDyYKY0o4gH567-fEJ3eme")
            },
            new Song
            {
                Name = "OH BITENTRONC",
                Image = "musique_Bitentronc.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Pour_Bitentronc.mp3",
                Url = new Uri("https://drive.google.com/open?id=1ynVsyobTCa8r-2AAhsXDKGanwvZKFZQH")
            },
            new Song
            {
                Name = "Oh Distingué",
                Image = "musique_distingue.png",
                Category = CategoryMusique,
                Mp3 = "musique/Oh-Distingue.mp3",
                Url = new Uri("https://drive.google.com/open?id=1jtX1s6XCdPmuCN1OqrDnHbWFCQOlPOWd")
            },
            new Song
            {
                Name = "Oh Patou",
                Image = "musique_Patou.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Oh-Patou.mp3",
                Url = new Uri("https://drive.google.com/open?id=11mSAMcZ46U4DHrFqL2PKwuXJ487zzW7O")
            },
            new Song
            {
                Name = "T'es tellement naze",
                Image = "musique_Tellementnaze.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/Gerald-de-Paldrong-T_es-tellement-naze.mp3",
                Url = new Uri("https://drive.google.com/open?id=1FzFGMZmbPiKY03tDaT9Z_5NVTmaK9NPB")
            },
            new Song
            {
                Name = "We are Patoche",
                Image = "musique_Patoche.PNG",
                Category = CategoryMusique,
                Mp3 = "musique/We-are-Patoche.mp3",
                Url = new Uri("https://drive.google.com/open?id=1WbTofumeqxuZ11tWNOdahUHgC6ZxxcSR")
            },
            new Song
            {
                Name = "La Poubelle aux Ernestine",
                Image = "La_Poubelle_aux_Ernestine.jpg",
                Category = CategoryMusique,
                Mp3 = "musique/La_Poubelle_aux_Ernestine.mp3",
                Url = new Uri("https://drive.google.com/file/d/1AuIOtaDRP0tzx4xMwUfoOYDJMvqDiXN2/view?usp=sharing")
            },
            new Song
            {
                Name = "Générique Pokemon ukulele",
                Image = "live_genie.png",
                Category = CategoryMusique,
                Mp3 = "live/generique_pokemon_ukulele.mp3",
                Url = new Uri("https://drive.google.com/file/d/13dC0Zn4fXkZMFb0b5gnSoclIebtftvj5/view?usp=sharing")
            },
            #endregion

            #region Empyrean
            new Song
            {
                Name = "Le moonblAAAst",
                Image = ImagePokemonEmpyrean,
                Category = CategoryLivePokemonEmpyrean,
                Mp3 = "Empyrean/le_moonblAAAst.mp3",
                Url = new Uri("https://drive.google.com/file/d/1bvTIpOdSyXPeARLqVnmv8zqPRFXt6rqJ/view?usp=sharing")
            },
	        #endregion


            #region Nouveaux

            new Song
            {
                Name = "Banquodrong CHEH",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/banquodrong_cheh.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ohTB4uYDYn4CZwHOLAuXJnZtB0Gz2-b4/view?usp=sharing")
            },
            new Song
            {
                Name = "Carnarequet a changé",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/carnarequet_a_change.mp3",
                Url = new Uri("https://drive.google.com/file/d/1z9CBhC-srnmId2XX3xuSP0u2i0DR3VMK/view?usp=sharing")
            },
            new Song
            {
                Name = "Dieu bitenfesse",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/dieu_bitenfesse.mp3",
                Url = new Uri("https://drive.google.com/file/d/18kOC9dAmhicvl2pn9EszMpccDGSoFGXQ/view?usp=sharing")
            },
            new Song
            {
                Name = "En train de vibre",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/en_train_de_vibre.mp3",
                Url = new Uri("https://drive.google.com/file/d/1eD0X8Xb5cXof6cnnMJTgF968vfx1vXvy/view?usp=sharing")
            },
            new Song
            {
                Name = "Eye of the fildrong",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/eye_of_the_fildrong.mp3",
                Url = new Uri("https://drive.google.com/file/d/17ZqrnXH328TokosgRwGw0tebWLRfcexj/view?usp=sharing")
            },
            new Song
            {
                Name = "Faut qu'on mette plumdanlcul",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/faut_quon_mette_plumdanlcul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1tWABFMpS5KzM23RZgOMSZw_2e8bZ-A4W/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildrong du sud",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/fildrong_du_sud.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Mv4a8k_Iuov0gCsfbuxDTdIOM1Etwet-/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildronglevrai sur instagram",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/fildronglevrai_sur_instagram.mp3",
                Url = new Uri("https://drive.google.com/file/d/1ZdCr8-KkaKnOOA_9DvUVQ7JNI7SueCCL/view?usp=sharing")
            },
            new Song
            {
                Name = "Fou rire",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/fou_rire.mp3",
                Url = new Uri("https://drive.google.com/file/d/1lXYCDY-AQ_qaGnCBwoiPd7Ty2yzH3wZK/view?usp=sharing")
            },
            new Song
            {
                Name = "Hate de te peter le cul",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/hate_de_te_peter_le_cul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1UVbnvpErnMnWHuJFG52NfSH-pY4SbTyp/view?usp=sharing")
            },
            new Song
            {
                Name = "Hommage",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/Hommage.mp3",
                Url = new Uri("https://drive.google.com/file/d/195v_W5PkT_1yCLTjtfQXFeB5TtmF-fbY/view?usp=sharing")
            },
            new Song
            {
                Name = "Il est malin",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/il_est_malin.mp3",
                Url = new Uri("https://drive.google.com/file/d/1CndGIj-AAa2gR4nMNLRHXSD3EkZvWeqn/view?usp=sharing")
            },
            new Song
            {
                Name = "Je dois déjà réfléchir",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/je_dois_deja_reflechir.mp3",
                Url = new Uri("https://drive.google.com/file/d/1fHGl6L2qTiaSQmmM5xYxO1VqiD6lDTCN/view?usp=sharing")
            },
            new Song
            {
                Name = "John salami",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/john_salami.mp3",
                Url = new Uri("https://drive.google.com/file/d/1SbmM-Fnu9tcfKaJh_xD5MlbOQnZMhHeh/view?usp=sharing")
            },
            new Song
            {
                Name = "Le coffret mode",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/le_coffret_mode.mp3",
                Url = new Uri("https://drive.google.com/file/d/1IQsAWuSLyHTa6fQOSBejzqY1i1XnaBdD/view?usp=sharing")
            },
            new Song
            {
                Name = "Médou",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/medou.mp3",
                Url = new Uri("https://drive.google.com/file/d/1rbeJWaVY8K5ev4-jpfxN7tevg0Awo-BY/view?usp=sharing")
            },
            new Song
            {
                Name = "Oh putaing",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/oh_putaing.mp3",
                Url = new Uri("https://drive.google.com/file/d/1-PUoxx9AFzWwvoACDRaVYSBoIH7B644k/view?usp=sharing")
            },
            new Song
            {
                Name = "Pas de bite",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/pas_de_bite.mp3",
                Url = new Uri("https://drive.google.com/file/d/1JPUN_CykkHaurCW9K7rjatjrmOlfWvIM/view?usp=sharing")
            },
            new Song
            {
                Name = "Petits coeurs",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/petits_coeurs.mp3",
                Url = new Uri("https://drive.google.com/file/d/1QGAsOBXuTsY0mGGQ_fNU2sqtEopsAfpc/view?usp=sharing")
            },
            new Song
            {
                Name = "Rage",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/rage_1.mp3",
                Url = new Uri("https://drive.google.com/file/d/1MoBcooUPoc75K9-SISCRTEJ2ldnShrEo/view?usp=sharing")
            },
            new Song
            {
                Name = "Switch dans le cul",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/switch_dansle_cul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1zxKWVjHJpb07WwyxAIkkhRgNS9TyWtQQ/view?usp=sharing")
            },
             new Song
            {
                Name = "Générique Pokemon ukulele",
                Image = "live_genie.png",
                Category = "Nouveaux",
                Mp3 = "live/generique_pokemon_ukulele.mp3",
                Url = new Uri("https://drive.google.com/file/d/13dC0Zn4fXkZMFb0b5gnSoclIebtftvj5/view?usp=sharing")
            },
            new Song
            {
                Name = "Oh les boobs",
                Image = "live_genie.png",
                Category = "Nouveaux",
                Mp3 = "live/oh_les_boobs.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Mlt9p79YyDsoYGmmImXc0gxsCiWs8Dyq/view?usp=sharing")
            },
            new Song
            {
                Name = "La vive griffe blizard",
                Image = ImageNewStrat,
                Category = "Nouveaux",
                Mp3 = "nouvellestrat/la_vive_griffe_blizard.mp3",
                Url = new Uri("https://drive.google.com/file/d/1eiu1PuUZeNnSLUQ8LKWwTv2nW8tpDewK/view?usp=sharing")
            },



            new Song
            {
                Name = "La malediction des fausses bites",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/la_malediction_des_fausses_bites.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Upy1m8tjasUhDAq9rizb9JoWLAQopXaS/view?usp=sharing")
            },
            new Song
            {
                Name = "Badges",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/badge.mp3",
                Url = new Uri("https://drive.google.com/file/d/1EMfjvBGwa5zEBcq8CD2gatidc5GSnnEO/view?usp=sharing")
            },
            new Song
            {
                Name = "A chaque fois",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/a_chaque_fois.mp3",
                Url = new Uri("https://drive.google.com/file/d/11wpV8N3PxSzPfx3J0g2Tyw4h-8MsbprI/view?usp=sharing")
            },
            new Song
            {
                Name = "Joli canapet",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/joli_canapet.mp3",
                Url = new Uri("https://drive.google.com/file/d/1l1qGR6RVwUG7KoM2Z7PWswDo_L2ZOCKV/view?usp=sharing")
            },
            new Song
            {
                Name = "Jvoulais juste briller V2",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/jvoulais_juste_briller_ver2.mp3",
                Url = new Uri("https://drive.google.com/file/d/1wnG-2QuYbBz2X1pufSAEZvwTVAoaUSaj/view?usp=sharing")
            },
            new Song
            {
                Name = "Mignon",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/mignon.mp3",
                Url = new Uri("https://drive.google.com/file/d/1gdopgFyxd-mMpO72jD6qcYadsEKJC1ux/view?usp=sharing")
            },
            new Song
            {
                Name = "Poing glace",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/poing_glace.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Xz_33yWOXAZjl0UJ1VLIW0zGSFV6CRU5/view?usp=sharing")
            },
            new Song
            {
                Name = "Explosion mentale",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/explosion_mentale.mp3",
                Url = new Uri("https://drive.google.com/file/d/1NnrRcbmLM77rtqC2PO9OtZWNONrMo21w/view?usp=sharing")
            },
            new Song
            {
                Name = "Jvoulais juste briller",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/jvoulais_juste_briller.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Mnuavuaat0XolEF-225_HdzV9TYEHxPW/view?usp=sharing")
            },
            new Song
            {
                Name = "Le mec qui a dit canon graine",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/le_mec_qui_a_dit_canon_graine.mp3",
                Url = new Uri("https://drive.google.com/file/d/11bPU6M4VNY6Fvlnx8HacYrCUumCHXRIE/view?usp=sharing")
            },
            new Song
            {
                Name = "Heureusement",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/heureusement.mp3",
                Url = new Uri("https://drive.google.com/file/d/1eoG70tprmyaVhgh78phGpiCGiJhwJ3a0/view?usp=sharing")
            },
            new Song
            {
                Name = "Caca qui pue la mort",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/caca_qui_pue_la_mort.mp3",
                Url = new Uri("https://drive.google.com/file/d/1-uuYCUDCrVtAL18bbZUhrPsEmAAfEFCh/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildrong russe",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/fildrong_russe.mp3",
                Url = new Uri("https://drive.google.com/file/d/1L_4T6X1La4YTTwJcKl5mYGF2GioYhQNG/view?usp=sharing")
            },
            new Song
            {
                Name = "Laves tes badges",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/laves_tes_badges.mp3",
                Url = new Uri("https://drive.google.com/file/d/1_KA7K4sqylhBjhze-NdADknWfzJ8mbSv/view?usp=sharing")
            },
            new Song
            {
                Name = "Le batard il est trop con",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/le_batard_il_est_trop_con.mp3",
                Url = new Uri("https://drive.google.com/file/d/1c6WWcyQi8-Wd92DtJduewqg5IRl68Gh3/view?usp=sharing")
            },
            new Song
            {
                Name = "Nettoyer mes toilettes",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/nettoyer_mes_toilettes.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Z3q6TR31zrStUSFMn1hyA7wHuTVFm6VF/view?usp=sharing")
            },
            new Song
            {
                Name = "Pisser du cul",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/pisser_du_cul.mp3",
                Url = new Uri("https://drive.google.com/file/d/1m-USZKTn4SxfPkWhh0B18sE1Flz2I7XP/view?usp=sharing")
            },
            new Song
            {
                Name = "Plaqué au sol",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/Plaque_au_sol.mp3",
                Url = new Uri("https://drive.google.com/file/d/1Ts-JXehrCh2r0ozX-3KQY96k6LIrFiWd/view?usp=sharing")
            },
            new Song
            {
                Name = "T'astiques bien",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/tastiques_bien.mp3",
                Url = new Uri("https://drive.google.com/file/d/1vEC4mz1gU7kgTSvHrfc-BlOteErnjVTp/view?usp=sharing")
            },
            new Song
            {
                Name = "Fini en a",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/fini_en_a.mp3",
                Url = new Uri("https://drive.google.com/file/d/1hfbCxffhpdyBJi3CiN-8u-Kdz0vvJadQ/view?usp=sharing")
            },
            new Song
            {
                Name = "Qui dit mort",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/qui_dit_mort.mp3",
                Url = new Uri("https://drive.google.com/file/d/1IkgFqw6zuqWoHgb1uZEOLxjTkGbVW8IS/view?usp=sharing")
            },
            new Song
            {
                Name = "Surf pour surfer",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/surf_pour_surfer.mp3",
                Url = new Uri("https://drive.google.com/file/d/140rzoLMj55mDPvuNxL21Hji3tu5Dmxuc/view?usp=sharing")
            },
            new Song
            {
                Name = "Rester calme",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/rester_calme.mp3",
                Url = new Uri("https://drive.google.com/file/d/14gCmeJraPnpZ18ffapn0l0GFMiI6XRx8/view?usp=sharing")
            },
            new Song
            {
                Name = "Bitenfesse mort",
                Image = ImagePokemonSaltyPlatinium,
                Category = "Nouveaux",
                Mp3 = "Salty/bitenfesse_mort.mp3",
                Url = new Uri("https://drive.google.com/file/d/1IiSkxYRWXuQSH1K9HBoTKOya4hW-fZQU/view?usp=sharing")
            },


            new Song
            {
                Name = "Tocopisco drong",
                Image = ImageNewStrat,
                Category = "Nouveaux",
                Mp3 = "nouvellestrat/tocopisco_drong.mp3",
                Url = new Uri("https://drive.google.com/file/d/10ywIiSAaDzIxznKHxb1Ajk5ViN9aO0Hr/view?usp=sharing")
            },
            new Song
            {
                Name = "Tadmorv drong",
                Image = ImageNewStrat,
                Category = "Nouveaux",
                Mp3 = "nouvellestrat/tadmorv_drong.mp3",
                Url = new Uri("https://drive.google.com/file/d/1AqapQ3htLS0VzNFzFkkaB3AMdJEJRIdq/view?usp=sharing")
            },
            new Song
            {
                Name = "Rage éboulement",
                Image = ImageNewStrat,
                Category = "Nouveaux",
                Mp3 = "nouvellestrat/rage_eboulement.mp3",
                Url = new Uri("https://drive.google.com/file/d/1yPLs2_v1-EL3bKk3no4w5RJ3tQXpKVCd/view?usp=sharing")
            },


            new Song
            {
                Name = "La préparation des vidéos de Fildrong",
                Image = "live_genie.png",
                Category = "Nouveaux",
                Mp3 = "live/la_preparation_des_videos_de_fildrong.mp3",
                Url = new Uri("https://drive.google.com/file/d/1k4sPNHWd7jZU6YKQQw2VdjV-QXyYOUkb/view?usp=sharing")
            },
            new Song
            {
                Name = "Fildrong et Among-us",
                Image = "live_genie.png",
                Category = "Nouveaux",
                Mp3 = "live/fildrong_et_among-us.mp3",
                Url = new Uri("https://drive.google.com/file/d/1yJOSOOa3yOJYm-ycxF3AmGZyhhDHoX7V/view?usp=sharing")
            },
            new Song
            {
                Name = "Robotdrong",
                Image = "live_genie.png",
                Category = "Nouveaux",
                Mp3 = "live/robotdrong.mp3",
                Url = new Uri("https://drive.google.com/file/d/1b9w0laOjzUE5fm3vjZXJpVbzJxQHzd_V/view?usp=sharing")
            },

            new Song
            {
                Name = "Un feu d'artifice avec ta grosse mère",
                Image = ImagePokemonX,
                Category = "Nouveaux",
                Mp3 = "X/un_feu_dartifice_avec_ta_grosse_mere.mp3",
                Url = new Uri("https://drive.google.com/file/d/1DvBjXX3_Nx9W5IiN-0B8PV4FJcHWPqXr/view?usp=sharing")
            },
            new Song
            {
                Name = "Rhinastoc",
                Image = ImagePokemonX,
                Category = "Nouveaux",
                Mp3 = "X/rhinastoc.mp3",
                Url = new Uri("https://drive.google.com/file/d/1VVWvyQihJXp-lOfuFCwxEi0Ru4gyedmg/view?usp=sharing")
            },

            new Song
            {
                Name = "Le moonblAAAst",
                Image = ImagePokemonEmpyrean,
                Category = "Nouveaux",
                Mp3 = "Empyrean/le_moonblAAAst.mp3",
                Url = new Uri("https://drive.google.com/file/d/1bvTIpOdSyXPeARLqVnmv8zqPRFXt6rqJ/view?usp=sharing")
            },

            #endregion
        };
        public static List<SongGroupCollection> SongsGroup { get; private set; } = new List<SongGroupCollection>{};
        #region init listsong
        static List<Song> ListSongAlerte = new List<Song>();
        static List<Song> ListSongImitations = new List<Song>();
        static List<Song> ListSongNouvelleStrat = new List<Song>();
        static List<Song> ListSongLiveDivers = new List<Song>();
        static List<Song> ListSongMusique = new List<Song>();
        static List<Song> ListSongCategory = new List<Song>();
        static List<Song> ListSongFavorite = new List<Song>();
        static List<Song> ListSongNouveaux = new List<Song>();

        static List<Song> ListSongLivePokemonSaltyPlatinium = new List<Song>();
        static List<Song> ListSongLivePokemonDonjonMystereExplorateurDuCiel = new List<Song>();
        static List<Song> ListSongLivePokemonDonjonMystereDX = new List<Song>();

        static List<Song> ListSongLivePokemonInfiniteFusion = new List<Song>();
        static List<Song> ListSongLivePokemonX = new List<Song>();
        static List<Song> ListSongLivePokemonEpee = new List<Song>();
        static List<Song> ListSongLivePokemonPlatineFullRandom = new List<Song>();
        static List<Song> ListSongLivePokemonClover = new List<Song>();
        static List<Song> ListSongLivePokemonBlazeBlack = new List<Song>();
        static List<Song> ListSongLivePokemonEclatPourpre = new List<Song>();
        static List<Song> ListSongLivePokemonMindCrystal = new List<Song>();
        static List<Song> ListSongLivePokemonEmeraudeFullRandom = new List<Song>();
        static List<Song> ListSongLivePokemonUltraSoleil = new List<Song>();
        static List<Song> ListSongLivePokemonVega = new List<Song>();
        static List<Song> ListSongLivePokemonInsurgence = new List<Song>();
        static List<Song> ListSongLivePokemonUranium = new List<Song>();
        static List<Song> ListSongLivePokemonEmpyrean = new List<Song>();

        #endregion

        public static void ListCategoryMaker()
        {
            Listsong = Listsong.OrderBy(f => f.Name).ToList();
            ListSongAlerte = Listsong.FindAll(x => x.Category == CategoryAlerte);
            ListSongImitations = Listsong.FindAll(x => x.Category == CategoryImitation);
            ListSongNouvelleStrat = Listsong.FindAll(x => x.Category == CategoryNewStrat);
            ListSongLiveDivers = Listsong.FindAll(x => x.Category == "Lives/Let's Play");
            ListSongMusique = Listsong.FindAll(x => x.Category == CategoryMusique);
            
            ListSongLivePokemonSaltyPlatinium = Listsong.FindAll(x => x.Category == CategoryLivePokemonSaltyPlatinium);
            ListSongLivePokemonDonjonMystereExplorateurDuCiel = Listsong.FindAll(x => x.Category == CategoryLivePokemonDonjonMystereExplorateurDuCiel);
            ListSongLivePokemonDonjonMystereDX = Listsong.FindAll(x => x.Category == CategoryLivePokemonDonjonMystereDX);
            ListSongLivePokemonInfiniteFusion = Listsong.FindAll(x => x.Category == CategoryLivePokemonInfiniteFusion);
            ListSongLivePokemonX = Listsong.FindAll(x => x.Category == CategoryLivePokemonX);
            ListSongLivePokemonEpee = Listsong.FindAll(x => x.Category == CategoryLivePokemonEpee);
            ListSongLivePokemonPlatineFullRandom = Listsong.FindAll(x => x.Category == CategoryLivePokemonPlatineFullRandom);
            ListSongLivePokemonClover = Listsong.FindAll(x => x.Category == CategoryLivePokemonClover);
            ListSongLivePokemonBlazeBlack = Listsong.FindAll(x => x.Category == CategoryLivePokemonBlazeBlack);
            ListSongLivePokemonEclatPourpre = Listsong.FindAll(x => x.Category == CategoryLivePokemonEclatPourpre);
            ListSongLivePokemonMindCrystal = Listsong.FindAll(x => x.Category == CategoryLivePokemonMindCrystal);
            ListSongLivePokemonEmeraudeFullRandom = Listsong.FindAll(x => x.Category == CategoryLivePokemonEmeraudeFullRandom);
            ListSongLivePokemonUltraSoleil = Listsong.FindAll(x => x.Category == CategoryLivePokemonUltraSoleil);
            ListSongLivePokemonVega = Listsong.FindAll(x => x.Category == CategoryLivePokemonVega);
            ListSongLivePokemonInsurgence = Listsong.FindAll(x => x.Category == CategoryLivePokemonInsurgence);
            ListSongLivePokemonUranium = Listsong.FindAll(x => x.Category == CategoryLivePokemonUranium);
            ListSongLivePokemonEmpyrean = Listsong.FindAll(x => x.Category == CategoryLivePokemonEmpyrean);
        }
        public static List<SongGroupCollection> GetSearchSoundboxResults(string queryString)
        {
            ListCategoryMaker();
            var normalizedQuery = queryString?.ToUpper() ?? "";
            #region liste
            ListSongAlerte = ListSongAlerte.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongImitations = ListSongImitations.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongNouvelleStrat = ListSongNouvelleStrat.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLiveDivers = ListSongLiveDivers.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongMusique = ListSongMusique.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            
            ListSongLivePokemonSaltyPlatinium = ListSongLivePokemonSaltyPlatinium.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonDonjonMystereExplorateurDuCiel = ListSongLivePokemonDonjonMystereExplorateurDuCiel.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonDonjonMystereDX = ListSongLivePokemonDonjonMystereDX.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonInfiniteFusion = ListSongLivePokemonInfiniteFusion.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonX = ListSongLivePokemonX.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonEpee = ListSongLivePokemonEpee.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonPlatineFullRandom = ListSongLivePokemonPlatineFullRandom.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonClover = ListSongLivePokemonClover.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonBlazeBlack = ListSongLivePokemonBlazeBlack.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonEclatPourpre = ListSongLivePokemonEclatPourpre.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonMindCrystal = ListSongLivePokemonMindCrystal.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonEmeraudeFullRandom = ListSongLivePokemonEmeraudeFullRandom.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonUltraSoleil = ListSongLivePokemonUltraSoleil.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonVega = ListSongLivePokemonVega.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonInsurgence = ListSongLivePokemonInsurgence.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonUranium = ListSongLivePokemonUranium.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            ListSongLivePokemonEmpyrean = ListSongLivePokemonEmpyrean.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            #endregion
            #region liste 2
            SongsGroup = new List<SongGroupCollection>();
            if (ListSongAlerte.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryAlerte, ListSongAlerte));
            }
            if (ListSongMusique.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryMusique, ListSongMusique));
            }
            if (ListSongImitations.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryImitation, ListSongImitations)); 
            }
            if (ListSongNouvelleStrat.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryNewStrat, ListSongNouvelleStrat));
            }
            if (ListSongLiveDivers.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection("Lives/Let's Play", ListSongLiveDivers));
            }
            

            if (ListSongLivePokemonSaltyPlatinium.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonSaltyPlatinium, ListSongLivePokemonSaltyPlatinium));
            }
            if (ListSongLivePokemonDonjonMystereDX.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonDonjonMystereDX, ListSongLivePokemonDonjonMystereDX));
            }
            if (ListSongLivePokemonDonjonMystereExplorateurDuCiel.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonDonjonMystereExplorateurDuCiel, ListSongLivePokemonDonjonMystereExplorateurDuCiel));
            }
            if (ListSongLivePokemonInfiniteFusion.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonInfiniteFusion, ListSongLivePokemonInfiniteFusion));
            }
            if (ListSongLivePokemonX.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonX, ListSongLivePokemonX));
            }
            if (ListSongLivePokemonEpee.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonEpee, ListSongLivePokemonEpee));
            }
            if (ListSongLivePokemonPlatineFullRandom.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonPlatineFullRandom, ListSongLivePokemonPlatineFullRandom));
            }
            if (ListSongLivePokemonClover.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonClover, ListSongLivePokemonClover));
            }
            if (ListSongLivePokemonBlazeBlack.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonBlazeBlack, ListSongLivePokemonBlazeBlack));
            }
            if (ListSongLivePokemonEclatPourpre.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonEclatPourpre, ListSongLivePokemonEclatPourpre));
            }
            if (ListSongLivePokemonMindCrystal.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonMindCrystal, ListSongLivePokemonMindCrystal));
            }
            if (ListSongLivePokemonEmeraudeFullRandom.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonEmeraudeFullRandom, ListSongLivePokemonEmeraudeFullRandom));
            }
            if (ListSongLivePokemonUltraSoleil.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonUltraSoleil, ListSongLivePokemonUltraSoleil));
            }
            if (ListSongLivePokemonVega.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonVega, ListSongLivePokemonVega));
            }
            if (ListSongLivePokemonInsurgence.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonInsurgence, ListSongLivePokemonInsurgence));
            }
            if (ListSongLivePokemonUranium.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonUranium, ListSongLivePokemonUranium));
            }
            if (ListSongLivePokemonEmpyrean.Count != 0)
            {
                SongsGroup.Add(new SongGroupCollection(CategoryLivePokemonEmpyrean, ListSongLivePokemonEmpyrean));
            }

            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            #endregion
            return SongsGroup;
        }

        public static List<SongGroupCollection> GetSearchSoundboxAlerteResults(string queryString, string category)
        {
            ListSongCategory = Listsong.FindAll(x => x.Category == category);
            var normalizedQuery = queryString?.ToUpper() ?? "";
            ListSongCategory = ListSongCategory.FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            SongsGroup = new List<SongGroupCollection>();
            SongsGroup.Add(new SongGroupCollection(category, ListSongCategory));

            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));

            return SongsGroup;
        }
        public static List<SongGroupCollection> GetSearchSoundboxFavoriteResults(string queryString)
        {
            SongDatabase songDB = new SongDatabase();
            var normalizedQuery = queryString?.ToUpper() ?? "";
            if (songDB.GetSong().ToList().Count == 0)
            {
                ListSongFavorite = new List<Song>();
            }
            else
            {
                ListSongFavorite = songDB.GetSong().ToList().FindAll(f => f.Name.ToUpperInvariant().Contains(normalizedQuery));
            }
            SongsGroup = new List<SongGroupCollection>();
            SongsGroup.Add(new SongGroupCollection("Favoris", ListSongFavorite));

            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));
            SongsGroup.Add(new SongGroupCollection("", new List<Song> { }));

            return SongsGroup;
        }
        
        public static void PlaySound(string x)
        {
            DependencyService.Get<ISoundPlayer>().PlaySoundFile(x);
        }
    }
}
