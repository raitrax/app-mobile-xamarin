﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Xamarin.Essentials;
using Xamarin.Forms;
using Fildrong.ViewModels;
using Fildrong.Models;
using System.Windows.Input;
using Fildrong.Services;
using System.Linq;

namespace Fildrong.Views
{
    [DesignTimeVisible(false)]
    public partial class SoundboxPage : ContentPage
    {
        bool huut = true;
        Song testteubé = new Song();
        string test1;
        string test2;
        SongDatabase songDatabase;
        SongDatabase songDB;
        Song songDBS = new Song();
        int ButtonCount;
        string rem;
        List<Song> FavoriteSong;

        public ICommand Command 
        {
            get { return new Command<Song> ((x) => Execute(x)); } 
        }
        void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (rem)
            {
                case "Alerte Live":
                case "Imitations":
                case "Nouvelle Strat":
                //case "Live Divers":
                case "Lives/Let's Play":
                case "Musique":
                case "Pokemon Salty Platinium":
                case "Pokemon Donjon Mystere Explorateur Du Ciel":
                case "Pokemon Donjon Mystere DX":
                case "Pokemon Infinite Fusion":
                case "Pokemon X":
                case "Pokemon Epee":
                case "Pokemon Platine Full Random":
                case "Pokemon Clover":
                case "Pokemon Blaze Black":
                case "Pokemon Eclat Pourpre":
                case "Pokemon Mind Crystal":
                case "Pokemon Emeraude Full Random":
                case "Pokemon Ultra Soleil":
                case "Pokemon Vega":
                case "Pokemon Insurgence":
                case "Pokemon Uranium":
                case "Pokemon Empyrean":
                case "Nouveaux":
                    SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxAlerteResults(e.NewTextValue, rem);
                    break;
                case "Favoris":
                    SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxFavoriteResults(e.NewTextValue);
                    break;
                default:
                    rem = "";
                    SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxResults(e.NewTextValue);
                    break;
            }
        }
        public SoundboxPage(string option)
        {
            InitializeComponent();
            Title = "Soundbox";
            switch (option)
            {
                case "Alerte Live":
                case "Imitations":
                case "Nouvelle Strat":
                case "Lives/Let's Play":
                //case "Live Divers":
                case "Musique":
                case "Pokemon Salty Platinium":
                case "Pokemon Donjon Mystere Explorateur Du Ciel":
                case "Pokemon Donjon Mystere DX":
                case "Pokemon Infinite Fusion":
                case "Pokemon X":
                case "Pokemon Epee":
                case "Pokemon Platine Full Random":
                case "Pokemon Clover":
                case "Pokemon Blaze Black":
                case "Pokemon Eclat Pourpre":
                case "Pokemon Mind Crystal":
                case "Pokemon Emeraude Full Random":
                case "Pokemon Ultra Soleil":
                case "Pokemon Vega":
                case "Pokemon Insurgence":
                case "Pokemon Uranium":
                case "Pokemon Empyrean":
                case "Nouveaux":
                    rem = option;
                    SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxAlerteResults(searchBarSoundBox.Text, rem);
                    break;
                case "Favoris":
                    rem = option;
                    SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxFavoriteResults(searchBarSoundBox.Text);
                    break;
                default:
                    rem = "";
                    SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxResults(searchBarSoundBox.Text);
                    break;
            }
        }
        public SoundboxPage()
        {
            InitializeComponent();
            Title = "Soundbox";
            SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxResults(searchBarSoundBox.Text);
        }

        private void Execute(Song y)
        {
            if (testteubé == y)
            {
                test2 = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
                ButtonCount++;
                if (ButtonCount > 1 && test1 != test2)
                {
                    test1 = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
                    ButtonCount = 1;
                }

                if (test1 == test2 && ButtonCount > 1)
                {
                    ButtonCount = 0;
                    songDB = new SongDatabase();
                    FavoriteSong = songDB.GetSong().ToList();
                    if (FavoriteSong.FindAll(z => z.Name == testteubé.Name).Count == 0)
                    {
                        songDatabase = new SongDatabase();
                        songDatabase.AddSong(testteubé);
                        DependencyService.Get<IToast>().Show("Favoris ajouté");
                        //DisplayAlert("", "Two Clicks " + ButtonCount + " OK " + test2, " OK");
                    }
                    else
                    {
                        songDBS = FavoriteSong.Find(z => z.Name == testteubé.Name);
                        //DisplayAlert("", "Two Clicks " + ButtonCount + " Pas OK " + test2, " OK");
                        DependencyService.Get<IToast>().Show("Favoris retiré");
                        songDatabase = new SongDatabase();
                        songDatabase.DeleteSong(songDBS.ID);
                        if (rem == "Favoris")
                        {
                            SoundBoxResult.ItemsSource = DataService.GetSearchSoundboxFavoriteResults(searchBarSoundBox.Text);
                        }
                    }
                }
            }
            else
            {
                testteubé = y;
                test1 = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
                ButtonCount = 1;
            }
            
            if (huut)
            {   
                DataService.PlaySound(y.Mp3);
            }
            else
            {
                Browser.OpenAsync(y.Url);
            }
        }

        private void ChangeMode(object sender, EventArgs e)
        {
            if (!huut)
            {
                switchButton.Text = "Mode Soundbox";
            }
            else
            {
                switchButton.Text = "Mode Download";
            }
            huut = !huut;
        }
        private void StopSound_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISoundPlayer>().StopSoundFile();
        }
        protected override bool OnBackButtonPressed()
        {
            DependencyService.Get<ISoundPlayer>().StopSoundFile();
            return base.OnBackButtonPressed();
        }
    }
}