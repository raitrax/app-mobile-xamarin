﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fildrong.Views
{
    [DesignTimeVisible(false)]
    public partial class AboutPageFriends : ContentPage
    {
        public AboutPageFriends()
        {
            InitializeComponent();
        }
    }
}