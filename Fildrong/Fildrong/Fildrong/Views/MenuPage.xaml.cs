﻿using Fildrong.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fildrong.Views
{
    [DesignTimeVisible(false)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }

        private readonly List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.Soundbox, Title="Soundbox" },
                new HomeMenuItem {Id = MenuItemType.AboutFriends, Title="Les amis" },
                new HomeMenuItem {Id = MenuItemType.AboutFildrong, Title="A propos de Fildrong" },
                new HomeMenuItem {Id = MenuItemType.About, Title="A propos de l'application" },
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id).ConfigureAwait(false);
            };
        }
    }
}