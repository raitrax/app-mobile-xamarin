﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fildrong.Views
{
    [DesignTimeVisible(false)]
    public partial class AboutPageFildrong : ContentPage
    {
        public AboutPageFildrong()
        {
            InitializeComponent();
        }
    }
}