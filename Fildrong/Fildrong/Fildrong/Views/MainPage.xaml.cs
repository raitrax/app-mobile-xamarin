﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Fildrong.Models;

namespace Fildrong.Views
{
    [DesignTimeVisible(false)]
    public partial class MainPage : MasterDetailPage
    {
        readonly Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        public MainPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;

            MenuPages.Add((int)MenuItemType.Browse, (NavigationPage)Detail);
        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuItemType.Soundbox:
                        MenuPages.Add(id, new NavigationPage(new TabbedSoundBoxPage()));
                        break;
                    case (int)MenuItemType.AboutFildrong:
                        MenuPages.Add(id, new NavigationPage(new AboutPageFildrong()));
                        break;
                    case (int)MenuItemType.AboutFriends:
                        MenuPages.Add(id, new NavigationPage(new AboutPageFriends()));
                        break;
                    case (int)MenuItemType.About:
                        MenuPages.Add(id, new NavigationPage(new AboutPage()));
                        break; 
                }
            }

            var newPage = MenuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100).ConfigureAwait(false);

                IsPresented = false;
            }
        }
    }
}