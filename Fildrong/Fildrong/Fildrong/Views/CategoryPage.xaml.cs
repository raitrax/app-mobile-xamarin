﻿using Fildrong.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fildrong.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryPage : ContentPage
    {
        public CategoryPage()
        {
            InitializeComponent();
        }
        private async void ButtonFavorite_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage("Favoris")).ConfigureAwait(false);
        }
        private async void ButtonNouveaux_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage("Nouveaux")).ConfigureAwait(false);
        }
        private async void ButtonAlerte_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryAlerte)).ConfigureAwait(false);
        }
        private async void ButtonImitations_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryImitation)).ConfigureAwait(false);
        }
        private async void ButtonNouvelleStrat_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryNewStrat)).ConfigureAwait(false);
        }
        private async void ButtonLivesDivers_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage("Lives/Let's Play")).ConfigureAwait(false);
            //await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLiveDivers)).ConfigureAwait(false);
        }
        private async void ButtonMusique_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryMusique)).ConfigureAwait(false);
        }

        private async void ButtonLivePokemonSaltyPlatinium_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonSaltyPlatinium)).ConfigureAwait(false);
        }
        private async void ButtonLivePokemonDonjonMystereExplorateurDuCiel_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonDonjonMystereExplorateurDuCiel)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonDonjonMystereDX_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonDonjonMystereDX)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonInfiniteFusion_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonInfiniteFusion)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonX_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonX)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonEpee_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonEpee)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonPlatineFullRandom_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonPlatineFullRandom)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonClover_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonClover)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonBlazeBlack_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonBlazeBlack)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonEclatPourpre_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonEclatPourpre)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonMindCrystal_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonMindCrystal)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonEmeraudeFullRandom_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonEmeraudeFullRandom)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonUltraSoleil_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonUltraSoleil)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonVega_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonVega)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonInsurgence_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonInsurgence)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonUranium_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonUranium)).ConfigureAwait(false);
        }
        private async void ButtonLivesPokemonEmpyrean_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SoundboxPage(DataService.CategoryLivePokemonEmpyrean)).ConfigureAwait(false);
        }
        protected override bool OnBackButtonPressed()
        {
            DependencyService.Get<ISoundPlayer>().StopSoundFile();
            return base.OnBackButtonPressed();
        }
    }
}