﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fildrong.ViewModels
{
    public class AboutFriendsViewModel : BaseViewModel
    {
        public AboutFriendsViewModel()
        {
            Title = "Les amis";
            //Red
            YoutubeRed = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UCAEhlJXdFkR9V5Sqeu-vptg")).ConfigureAwait(false));

            //Maitre Armand
            YoutubeMA = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UCysbNzhNmpLCx4dqWNFzGgA")).ConfigureAwait(false));

            //Sorcier Malgache
            YoutubeSM = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UC9E3zLroMFLvvTZt7JJyq_A")).ConfigureAwait(false));

            //Hari
            YoutubeHa = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UCV1aAmztXWk-uM-nw57hFLQ")).ConfigureAwait(false));

            //Pokemon Trash
            YoutubePT = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/user/videopokemontrash")).ConfigureAwait(false));

            //Daniel V
            YoutubeDV = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UCJo_r78fq2hu8UY3L9wI7sg")).ConfigureAwait(false));

            //Sundae
            YoutubeSundae = new Command(async () => await Browser.OpenAsync(new Uri("https://twitter.com/LADYSUNDAE/")).ConfigureAwait(false));
            
            //Redeon
            YoutubeRedeon = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UCui_HR8oE3JGR96vDCAo2Tw")).ConfigureAwait(false));
        }

        //Red
        public ICommand YoutubeRed { get; }

        //Sorcier
        public ICommand YoutubeSM { get; }

        //Armand
        public ICommand YoutubeMA { get; }

        //HARI
        public ICommand YoutubeHa { get; }

        //Pokemon Trash
        public ICommand YoutubePT { get; }
        
        //Daniel V
        public ICommand YoutubeDV { get; }

        //Sundae
        public ICommand YoutubeSundae { get; }

        //Redeon (secondaire à Redemption)
        public ICommand YoutubeRedeon { get; }

    }
}