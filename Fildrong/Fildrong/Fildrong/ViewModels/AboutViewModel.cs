﻿using Fildrong.Views;
using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fildrong.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "A Propos";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync(new Uri("https://discord.gg/ePeUV3U")).ConfigureAwait(false));
            OpenWebCommand2 = new Command(async () => await Browser.OpenAsync(new Uri("https://twitter.com/haestiaa")).ConfigureAwait(false));
            //OpenWebCommand3 = new Command(async () => await Browser.OpenAsync(new Uri("https://www.paypal.me/raitrax")).ConfigureAwait(false));
        }

        public ICommand OpenWebCommand { get; }
        public ICommand OpenWebCommand2 { get; }
        //public ICommand OpenWebCommand3 { get; }
    }
}