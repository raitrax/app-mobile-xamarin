﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fildrong.ViewModels
{
    public class AboutFildrongViewModel : BaseViewModel
    {
        public AboutFildrongViewModel()
        {
            Title = "A propos de Fildrong";
            Youtube = new Command(async () => await Browser.OpenAsync(new Uri("https://www.youtube.com/channel/UCO35rCYrO_3t6f4tEU83WKg")).ConfigureAwait(false));
            Twitter = new Command(async () => await Browser.OpenAsync(new Uri("https://twitter.com/Fildrong")).ConfigureAwait(false));
            Twitch = new Command(async () => await Browser.OpenAsync(new Uri("https://www.twitch.tv/fildrong")).ConfigureAwait(false));
            Instagram = new Command(async () => await Browser.OpenAsync(new Uri("https://www.instagram.com/fildronglevrai")).ConfigureAwait(false));
            Facebook = new Command(async () => await Browser.OpenAsync(new Uri("https://www.facebook.com/fildrong/")).ConfigureAwait(false));
            Discord = new Command(async () => await Browser.OpenAsync(new Uri("https://discord.gg/fildrong")).ConfigureAwait(false));
            Showdown = new Command(async () => await Browser.OpenAsync(new Uri("http://play.pokemonshowdown.com/salty")).ConfigureAwait(false));
            Teespring = new Command(async () => await Browser.OpenAsync(new Uri("https://teespring.com/stores/fildrong")).ConfigureAwait(false));
            BestOfFildrong = new Command(async () => await Browser.OpenAsync(new Uri("https://twitter.com/OOC_Fildrong")).ConfigureAwait(false));
            UnFildrongParJour = new Command(async () => await Browser.OpenAsync(new Uri("https://twitter.com/DailyFildrong")).ConfigureAwait(false));
        }

        public ICommand Youtube { get; }
        public ICommand Twitter { get; }
        public ICommand Twitch { get; }
        public ICommand Instagram { get; }
        public ICommand Facebook { get; } 
        public ICommand Discord { get; }
        public ICommand Showdown { get; }
        public ICommand Teespring { get; }
        public ICommand BestOfFildrong { get; }
        public ICommand UnFildrongParJour { get; }

    }
}