﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fildrong
{
    public interface ISQLite
    {
        SQLite.SQLiteConnection GetConnection();
    }
}
